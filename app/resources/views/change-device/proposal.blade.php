@extends('layouts.admin')

@section('title')
    Proposal - Ganti Perangkat Akses Lampung Online
@endsection
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <input type="hidden" name="page" id="page" value="1">
                <table class="display table">
                    <head>
                        <th>Nama Nasabah</th>
                        <th>Keterangan</th>
                        <th>Status</th>
                        <th>Date Create</th>
                        <th>CS Name</th>
                        <th>Action</th>
                    </head>
                    <body>
                        @foreach($datas as $data)
                            <tr>
                                <td>{{ $data->customer->full_name ?? '-' }}</td>
                                <td>{{ $data->application ?? '-' }}</td>
                                <td>{{ $data->proposal_status ?? '-' }}</td>
                                <td>{{ $data->proposal_created_at ?? '-' }}</td>
                                <td>{{ $data->created_by->full_name ?? '-' }}</td>
                                <td>
                                    @if($data->proposal_status == 'PROPOSAL_CREATED' && isOtorisator())
                                    <a href="{{route('changeDevice.detail', $data->id )}}" class="btn btn-warning">Aprove / Reject</a>
                                    @elseif(($data->proposal_status == 'PROPOSAL_CREATED' || $data->proposal_status == 'FINISHED') && isInitiator())
                                    <a href="{{route('changeDevice.detail', $data->id )}}" class="btn btn-success">Detail</a>
                                    @elseif($data->proposal_status == 'DRAFT_CREATED' && isInitiator()) 
                                    <a href="{{route('changeDevice.detail', $data->id )}}" class="btn btn-success">Detail</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    </body>
                </table>
                
                @include('component.pagination', [
                    'page'      => $page,
                    'paging'    => $paging
                ])
            </div>
        </div>
    </div>
</div>
@endsection