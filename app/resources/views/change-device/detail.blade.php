@extends('layouts.admin')

@section('title')
    Detail Proposal - Ganti Perangakat Akses Lampung Online
@endsection
@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                @if($data->proposal_status == 'FINISHED')
                <b>Status: </b>
                <span class="btn-sm btn-success">FINISH</span>
                @endif
            </div>
            <div class="card-body">

                <!-- Start: NIK -->
                <div class="form-group">
                    <label> NIK Nasabah</label>
                    <input type="text" class="form-control" value="{{ substr($data->customer->id_card_number, 0, 10) . 'xxxxxx' }}" id="nik" disabled>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >No. Kartu Keluarga</label>
                            <p class="form-control-static">{{ substr($data->customer->family_card_number ?? '000000000000000', 0, 10) . 'xxxxxx' }}</p>
                        </div>
                        <div class="form-group">
                            <label >Nama Lengkap</label>
                            <p class="form-control-static">{{$data->customer->full_name}}</p>
                        </div>
                        <div class="form-group">
                            <label >Jenis Kelamin</label>
                            <p class="form-control-static">{{$data->customer->gender}}</p>
                        </div>
                        <div class="form-group">
                            <label >Pendidikan Terakhir</label>
                            <p class="form-control-static">{{$data->customer->education}}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Tanggal Lahir</label>
                            <p class="form-control-static">{{$data->customer->birth_place}}, {{$data->customer->birth_date}}</p>
                        </div>
                        <div class="form-group">
                            <label >Nama Ibu Kandung</label>
                            <p class="form-control-static">{{$data->customer->mother_name}}</p>
                        </div>
                        <div class="form-group">
                            <label >Agama</label>
                            <p class="form-control-static">{{$data->customer->religion}}</p>
                        </div>
                        <div class="form-group">
                            <label >Pekerjaan</label>
                            <p class="form-control-static">{{$data->customer->job_type}}</p>
                        </div>
                    </div>
                </div>
            </div>
            @include('component.submit-proposal.index')
        </div>
    </div>
</div>
@endsection

@push('script')
    <!-- Sweetalert2 -->
    <script src="{{ asset('assets/sweetalert2/sweetalert2.all.min.js') }}"></script>
    @include('component.submit-proposal.js', [
        'id' => $id,
        'routeName' => 'otorisator.aprovalProposal'
    ])
@endpush
