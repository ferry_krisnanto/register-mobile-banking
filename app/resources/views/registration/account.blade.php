@extends('layouts.admin')

@section('title')
    Step 3 :: Pendaftaran - Mencari Akun Bank Berdasarkan CIF
@endsection

@section('content')
@if ($message = Session::get('failed'))
    <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
    </div>
@endif
@if(!empty($account))
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Nama Lengkap</label>
                            <p class="form-control-static">{{ $account->customer->full_name ?? ''}}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >NIK</label>
                            <p class="form-control-static">{{ $account->customer->id_card_number  ?? ''}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
@if(!empty($account->additional_data->saving_accounts))
    <h3>List Rekening Yang Sudah di Daftarkan</h3>
    @foreach( $account->additional_data->saving_accounts as $list)
        <label class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ $list->product_name }}</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <label>Name: {{ $list->account_name ?? ''}}</label><br>
                                    <label>Status: {{ $list->customer_status_name ?? '' }}</label><br>
                                    <label>Nomor Rekening: {{ $list->account_number ?? ''}}</label><br>
                                    <label>Jenis Rekening: {{ $list->product_code ?? '' . ' - ' .$list->product_name ?? ''}}</label><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <span class="checkmark"></span>
        </label>
    @endforeach
@endif
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>Note: Masukan Nomor CIF Nasabah, Pastikan </strong>
                            <p>1: CIF BENAR</p>
                            <p>2: REKENING SESUAI</p>
                            <p>3: PILIH SALAH SATU REKENING, Atau BISA BEBERAPA REKENING SEKALIGUS yang ingin DI AKTIFKAN</p>
                            <p>-: Apabila Terjadi Kesalahan Sistem (Error), Mohon untuk di Foto dan Hubungi : <b><u>DIV TI BANK LAMPUNG</u></b></p>
                        </div>
                        <div class="form-group">
                            <div class="row">

                                <div class="col-md-6">
                                    <label for="">Nomor Rekening</label>
                                    <div class="input-group" id="inquiry-nik">
                                        <input type="text" class="form-control" id="account_number" name="account_number" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <label for="">CIF Nasabah</label>
                                    <div class="input-group" id="inquiry-nik">
                                        <input type="text" class="form-control" id="cif" name="cif" required>
                                        <div class="input-group-prepend">
                                            <button class="btn btn-primary" type="button" id="button-cif" placeholder="Masukan CIF">Mencari CIF</button>
                                        </div>
                                    </div>
                                </div>
                            
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" id="form"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script src="{{ asset('assets/sweetalert2/sweetalert2.all.min.js') }}"></script>
<script>    
    $("#button-cif").click(function(){
        var cif = $('#cif').val();
        var account_number = $('#account_number').val();
        if(cif.length == 10){
            swal({
                title: "Mohon Tunggu",
                showConfirmButton: false,
                allowOutsideClick: false,
            });
            getAccountByCif(cif, account_number);
        }else{
            swal({
                title: "Error",
                text: "CIF :: Tidak Boleh Kosong dan Minimal 10 Character",
                type: "error",
                allowOutsideClick: false,
            });
        }
        
    });
    function getAccountByCif(cif, account_number) {
        $.ajax({
            url: "{{ route('Register.getAccountByCif') }}",
            type:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'cif': cif,
                'account_number': account_number,
                'draft_id': "{{$draftId}}",
            },
            success: function(response){
                swal.close()
                $("#form").html(response);
            },
            error: function (response) {
                swal({
                    title: 'Something Wrong',
                    text: response.responseJSON,
                    type: "error",
                    allowOutsideClick: false,
                });
                console.log(response);
            }
        });
    }
</script>
@if ($message = Session::get('failed'))
    <script>
        swal({
            title: "Error",
            text: "Account :: {{ $message }}",
            type: "error",
            allowOutsideClick: false,
        });
    </script>
@endif
@endpush
