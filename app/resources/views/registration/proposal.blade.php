@extends('layouts.admin')

@section('title')
    Proposal :: Registarasi
@endsection

@push('css')
<style>
    .custom>tbody>tr>td, .custom>tbody>tr>th, .custom>thead>tr>td, .custom>thead>tr>th{
        padding: -3px !important;
    }
</style>
@endpush

@section('content')

@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
    </div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <input type="hidden" name="page" id="page" value="1">
                <table class="display table">
                    <head>
                        <th>Name</th>
                        <th>Id Card</th>
                        <th>Type</th>
                        <th>Desc</th>
                        <th>Status</th>
                        <th>JMl Rekening</th>
                        <th>Action</th>
                    </head>
                    <body>
                        @foreach($datas as $data)
                            <tr>
                                <td>{{ $data->customer->full_name ?? '-' }}</td>
                                <td>{{ isset($data->customer->id_card_number) ? substr($data->customer->id_card_number, 0, 10) . 'xxxxxx' : '-' }}</td>
                                <td>{{ $data->doc_type ?? '-' }}</td>
                                <td>{{ $data->application ?? '-' }}</td>
                                <td>{{ $data->proposal_status ?? '-' }}</td>
                                <td>{{ isset($data->additional_data->saving_accounts) ? count($data->additional_data->saving_accounts) : '-' }}</td>
                                <td>
                                    @if($data->proposal_status == 'FINISHED')
                                    <a href="{{route('list.id', $data->id )}}" class="btn btn-success">Detail</a></td>
                                    @elseif($data->proposal_status == 'PROPOSAL_CREATED' && isOtorisator())
                                    <a href="{{route('list.id', $data->id )}}" class="btn btn-warning">Aprove / Reject</a></td>
                                    @elseif($data->proposal_status == 'PROPOSAL_CREATED' && isInitiator())
                                    <a href="{{route('list.id', $data->id )}}" class="btn btn-success">Detail</a></td>
                                    @elseif($data->proposal_status == 'DRAFT_CREATED' && isInitiator()) 
                                    <!-- <a href="{{route('list.id', $data->id )}}" target="_blank" rel="noopener noreferrer" class="btn btn-primary">Lanjutkan</a></td> -->
                                    <a href="{{route('list.checkStep', $data->id )}}" class="btn btn-primary">Lanjutkan</a></td>
                                    @endif
                            </tr>
                        @endforeach
                    </body>
                </table>
                @include('component.pagination', [
                    'page'      => $page ?? 1,
                    'paging'    => $paging ?? 1
                ])
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    var page = $('#page').val();
    console.log(page);
</script>
@endpush
