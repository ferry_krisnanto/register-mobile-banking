@extends('layouts.admin')

@section('title')
    Step 4 :: Registarasi - Pilih Otorisator
@endsection

@section('content')
<div>
    <div class="card">
        <div class="card-header">
            <h4>Silahkan Pilih Atasan/SVP/Pimpinan Cabang Sebagai Penyetuju</h4>
        </div>
    </div>

    <form action="{{route('Register.submit', $draftId)}}" method="POST">
        @csrf
        <input type="hidden" name="draft_id" value="{{$draftId}}">
        @include('component.otorisator', [
            'users' => $users
        ])
        <br>
        <button class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection