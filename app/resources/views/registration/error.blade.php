@extends('layouts.admin')
@section('title')
    Erorr :: Registarasi
@endsection
@push('script')
<script src="{{ asset('assets/sweetalert2/sweetalert2.all.min.js') }}"></script>
<script>
    swal({
        title: "{{$response->code}}",
        text: "{{$response->message->indonesian ?? json_encode($response) }}",
        type: "error",
        allowOutsideClick: false,
        showConfirmButton: false,
    });
</script>
@endpush