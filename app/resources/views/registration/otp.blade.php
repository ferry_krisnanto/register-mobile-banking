@extends('layouts.admin')

@section('title')
    Step 2 :: Registarasi - Pendaftaran dan Verifikasi No HP Nasabah
@endsection

@push('css')
<style>
    .custom>tbody>tr>td, .custom>tbody>tr>th, .custom>thead>tr>td, .custom>thead>tr>th{
        padding: -3px !important;
    }
</style>
<style>
    .loader {
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite; /* Safari */
    animation: spin 2s linear infinite;
    }

    /* Safari */
    @-webkit-keyframes spin {
    0% { -webkit-transform: rotate(0deg); }
    100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
    }
</style>
@endpush

@section('content')
@if(!empty($data))
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Nama Lengkap</label>
                            <p class="form-control-static">{{$data['name'] ?? ''}}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >NIK</label>
                            <p class="form-control-static">{{$data['nik'] ?? ''}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>Note: Masukan Nomor HP Nasabah, Pastikan Nomor tersebut </strong>
                            <p>1: Nomor Nasabah dalam Kondisi Aktif dan Ada Sinyal</p>
                            <p>2: Untuk Nomor Prabayar Minimal Pulsa Rp. 5.000</p>
                            <p>3: Untuk Nomor Pascabayar Sisa Limit Minimal Rp. 5.000</p>
                            <p>4: Jika Waktu Tunggu Send OTP Habis, Masih Bisa Validasi OTP Karena Masa Waktu Tunggu OTP Paling Lama 5 Menit</p>
                            <p>5: Apabila Terjadi Kesalahan Sistem (Error), Mohon untuk di Foto dan Hubungi : <b><u>DIV TI BANK LAMPUNG</u></b></p>
                        </div>
                            
                        <div class="card" id="inquiry-nik">
                            <div class="card-header">
                                <label ><b>Masukan Nomor HandPhone Nasabah</b></label>
                            </div>
                            <div class="card-body">
                                <div class="input-group">
                                    <input type="text" class="form-control" id="phone" name="phone" min="15" max="16" required placeholder="Masukan Nomor HP: 08xxxxxxxx">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-primary" type="button" id="button-send-otp" >Send OTP</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" id="counter" hidden> 
                        <div class="card full-height">
                            <div class="card-body">
                                <div class="card-title">Waktu Tunggu untuk dapat mengirimkan ulang Kode OTP lagi</div>
                                    <div class="d-flex flex-wrap justify-content-around pb-2 pt-4">
                                        <div class="px-2 pb-2 pb-md-0 text-center">
                                            <div id="circles-2">
                                                <div class="circles-wrp" style="position: relative; display: inline-block;">
                                                <svg xmlns="http://www.w3.org/2000/svg" width="90" height="90">
                                                    <path fill="transparent" stroke="#2BB930" stroke-width="7" d="M 44.99154756204665 3.500000860767564 A 41.5 41.5 0 1 1 44.942357332570026 3.500040032273624 " class="circles-valueStroke"></path>
                                                </svg>
                                                    <div class="circles-text" style="position: absolute; top: 0px; left: 0px; text-align: center; width: 100%; font-size: 31.5px; height: 90px; line-height: 90px;">
                                                        <div id="countdowntimer">60</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <h6 class="fw-bold mt-3 mb-0">SEND OTP</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="card" id="validate" hidden>
                        <div class="card-header">
                            <label ><b>Masukan Kode OTP dari Nasabah</b></label>
                        </div>
                        <div class="card-body">
                            <div class="form-group">
                                <div class="input-group" id="inquiry-nik">
                                    <input type="text" class="form-control" id="otp-code" name="otp_code" required placeholder="Masukan 6 Digit Kode OTP">
                                    <div class="input-group-prepend">
                                        <button class="btn btn-primary" type="button" id="button-verif-otp" >Verifikasi OTP</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <div id="successValidate" hidden>
                        <div class="col-md-2 ml-auto mr-auto">
                            <a href="{{route('RegisterInquiry.step3', $draftId)}}" class="btn btn-primary">Lanjutkan Step 3</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script src="{{ asset('assets/sweetalert2/sweetalert2.all.min.js') }}"></script>
<script>
    $("#button-send-otp").click(function(){
        swal({
            title: "Mohon Tunggu",
            allowOutsideClick: false,
        });
        sendOtp();
    });

    function sendOtp() {
        document.getElementById("inquiry-nik").hidden = true;
        $.ajax({
            url: "{{ route('send.otp') }}",
            type:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'phone': $('#phone').val(),
                'draft_id': "{{$draftId}}",
            },
            success: function(response){
                swal({
                    text: "OTP Berhasil Di Kirimkan, Tunggu 60 detik untuk Mengirimkan Kembali",
                    type: "success",
                    allowOutsideClick: false,
                });
                $("#form").html(response);
                document.getElementById("counter").hidden = false;
                document.getElementById("validate").hidden = false;

                var timeleft = 60;
                var downloadTimer = setInterval(function(){
                timeleft--;
                document.getElementById("countdowntimer").textContent = timeleft;
                    if(timeleft <= 0){
                        clearInterval(downloadTimer);
                        document.getElementById("inquiry-nik").hidden = false;
                        document.getElementById("counter").hidden = true;
                    }
                },1000);
            },
            error: function (response) {
                errorShow(response.responseJSON);
                document.getElementById("inquiry-nik").hidden = false;
            }
        });
    }

    function errorShow(response){
        swal({
            title: response.code,
            text: response.message && response.message.indonesian,
            type: "error",
            allowOutsideClick: false,
        });
    }
</script>
<script>
    $("#button-verif-otp").click(function(){
        swal({
            title: "Mohon Tunggu",
            showConfirmButton: false,
            allowOutsideClick: false,
        });
        validateOtp();
    });

    function validateOtp() {
        $.ajax({
            url: "{{ route('RegisterInquiry.validateOtp') }}",
            type:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'otp_code': $('#otp-code').val(),
                'draft_id': "{{ $draftId }}",
            },
            success: function(response){
                if(response.data.is_valid){
                    swal({
                        title: "No HP Berhasil di Validasi",
                        text: "Silahkan Lanjutkan Step 3. Dengan klik tombol Lanjutkan",
                        type: "success",
                        allowOutsideClick: false,
                    });
                    document.getElementById("successValidate").hidden = false;
                }else{
                    swal({
                        title: response.code,
                        text: response.data.reason,
                        type: "error",
                        allowOutsideClick: false,
                    });
                }
            },
            error: function (response) {
                console.log(response);
                errorShow(response.responseJSON);
            }
        });
    }
</script>
@endpush