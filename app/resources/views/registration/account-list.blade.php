<div>
<div class="card">
    <div class="card-header">
        <h4>Silahkan Ceklis Account Rekening yang Ingin di Aktifkan</h4>
    </div>
</div>

<form action="{{route('Register.saveAccount')}}" method="post">
    @csrf
    @if(isset($listAllAccount->data->saving_accounts))
    <input type="hidden" name="draft_id" value="{{$draftId}}">
    <input type="hidden" name="customer_branch" value="{{ $listAllAccount->data->customer_branch }}">
    <input type="hidden" name="customer_number" value="{{ $listAllAccount->data->customer_number }}">
    @foreach($listAllAccount->data->saving_accounts as $account)
        @if( in_array($account->product_code, config('static.product')) && $account->customer_status_name == 'AKTIF' )
        <label class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">{{ $account->product_name }}</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <label>Name: {{ $account->account_name}}</label><br>
                                    <label>Status: {{ $account->customer_status_name }}</label><br>
                                    <label>Nomor Rekening: {{ $account->account_number }}</label><br>
                                    <label>Jenis Rekening: {{ $account->product_code . ' - ' .$account->product_name }}</label><br>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <br>
                                @if(in_array($account->account_number, $hasRegisteredAccount))
                                    <span style="float:right" class="btn btn-success">MBanking - Sudah Aktif</span>
                                @else
                                <input style="float:right; transform: scale(2.0);" name="account[]" type="checkbox" value="{{ $account->account_number }}">
                                @endif
                            </div>
                            <div class="col-md-1"></div>
                        </div>
                        
                    </div>
                </div>
            </div>
            <span class="checkmark"></span>
        </label>
        @endif
    @endforeach
    <br>
    <script>
        function submitAccount(){
            swal({
                title: "Mohon Tunggu",
                showConfirmButton: false,
                allowOutsideClick: false,
            });
        }
    </script>
    <button type="submit" class="btn btn-primary" onclick="submitAccount()">Submit</button>
    @endif
</form>
</div>