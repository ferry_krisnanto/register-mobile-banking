@extends('layouts.admin')

@section('title')
    Step 1 :: Registarasi - Pengisian Form
@endsection

@section('content')

    @include('component.registration.inquiry')
    @include('component.registration.form')

@endsection

@push('script')
<script src="{{ asset('assets/sweetalert2/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('assets/select2/select2.full.min.js') }}"></script>
<script>
    $("#button-inquiry-nik").click(function(){
        getInquiry();
        swal({
            title: "Mohon Tunggu",
            text: "Permintaan Sedang Di Proses",
            allowOutsideClick: false,
            showConfirmButton: false,
        });
    });
    $("#button-save").click(function(){
        var valid = $("#form").valid();
        if(valid){
            swal({
                title: "Mohon Tunggu",
                text: "Permintaan Sedang Di Proses",
                allowOutsideClick: false,
                showConfirmButton: false,
            });
        }
    });
    function getInquiry() {
        $.ajax({
            url: "{{ route('RegisterInquiry.inquiry') }}",
            type:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'nik': $('#nik').val(),
            },
            success: function(response){
                if(response.success){
                    swal({
                        title: response.code,
                        text: 'NIK Valid',
                        type: "success",
                        allowOutsideClick: false,
                    });
                    setToForm(response.data);
                    document.getElementById("button-save").hidden = false;
                }else{
                    errorShow(response);
                }
            },
            error: function (response) {
                errorShow(response.responseJSON);
            }
        });
    }
    function setToForm(data){
        var birthDay = reverseString(data.birth_date);

        $('#nik_identity').val(data.nik);
        $('#nik_nasabah').val(data.nik);
        $('#full_name').val(data.full_name);
        $('#gender').val(data.gender);
        $('#marital_status').val(data.marital_status)
        $('#education').val(data.education);
        $('#place_of_birth').val(data.birth_place);
        $('#date_of_birth').val(birthDay);
        $('#mother_name').val(data.mother_name);
        $('#religion').val(data.religion);
        $('#job').val(data.job);
        $('#family_card').val(data.family_card_number)
        
        $('#address').val(data.domicile.address);
        $('#province_name').val(data.domicile.province_name);
        $('#city_name').val(data.domicile.regency_name);
        $('#district_name').val(data.domicile.district_name);
        $('#sub_district_name').val(data.domicile.village_name);
        $('#rt_number').val(data.domicile.hamlet_number);
        $('#rw_number').val(data.domicile.neighbourhood_number);
    }
    function reverseString(str) {
        return str.split("-").reverse().join("-");
    }

    function errorShow(response){
        swal({
            title: response.code,
            text: response.message && response.message.indonesian,
            type: "error",
            allowOutsideClick: false,
        });
    }

    function getProvince(){
        $.ajax({
            url: "{{ route('RegisterInquiry.inquiry') }}",
            type:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'nik': $('#nik').val(),
            },
            success: function(response){
                if(response.success){
                    swal({
                        title: response.code,
                        text: 'Berhasil Di Temukan',
                        type: "success",
                        allowOutsideClick: false,
                    });
                    setToForm(response.data);
                }else{
                    errorShow(response);
                }
            },
            error: function (response) {
                errorShow(response.responseJSON);
            }
        });
    }
</script>
<script>
    $('#checkbox').change(function() {
        if(this.checked) {
            $('#address-domicily').hide();
            
            $("#address_dom").removeAttr('required');
            $("#province_name_dom").removeAttr('required');
            $("#district_name_dom").removeAttr('required');
            $("#rt_number_dom").removeAttr('required');
            $("#city_name_dom").removeAttr('required');
            $("#sub_district_name_dom").removeAttr('required');
            $("#rw_number_dom").removeAttr('required');
        }else{
            $('#address-domicily').show();

            $("#address_dom").attr('required', '');
            $("#province_name_dom").attr('required', '');
            $("#district_name_dom").attr('required', '');
            $("#rt_number_dom").attr('required', '');
            $("#city_name_dom").attr('required', '');
            $("#sub_district_name_dom").attr('required', '');
            $("#rw_number_dom").attr('required', '');
        }      
    });
</script>
@endpush