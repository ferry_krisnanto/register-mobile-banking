<li class="nav-item {{ setActive(['home']) }}">
    <a href="{{ route('home') }}">
        <i class="fas fa-home"></i>
        <p>Dashboard</p>
    </a>
</li>

@if(isInitiator())
    @include('layouts.menu.cs')
@endif

@if(isOtorisator())
    @include('layouts.menu.aproval')
@endif

<!-- <li class="nav-item">
    <a href="{{ route('list.index') }}">
        <i class="fas fa-bars"></i>
        <p>List PROPOSAL</p>
    </a>
</li> -->

<li class="nav-item">
    <a data-toggle="collapse" href="#proposal" class="collapsed" aria-expanded="false">
        <i class="fas fa-folder-open"></i>
        <p>List Proposal</p>
        <span class="caret"></span>
    </a>
    <div class="collapse" id="proposal" style="">
        <ul class="nav nav-collapse">
            <li>
                <a href="{{ route('list.index') }}">
                    <span class="sub-item">Proposal Registrasi</span>
                </a>
            </li>
            <li>
                <a href="{{ route('changePin.proposals') }}">
                    <span class="sub-item">Proposal Reset PIN</span>
                </a>
            </li>
            <li>
                <a href="{{ route('changePhone.proposals') }}">
                    <span class="sub-item">Proposal Ganti No. HP</span>
                </a>
            </li>
            <li>
                <a href="{{ route('blokir.proposals') }}">
                    <span class="sub-item">Proposal Blokir Akses</span>
                </a>
            </li>
            
        </ul>
    </div>
</li>
