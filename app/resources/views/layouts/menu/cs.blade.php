<li class="nav-item">
    <a href="{{ route('register.index') }}">
        <i class="fas fa-mobile-alt"></i>
        <p>Register M-Banking</p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('changePin.index') }}">
        <i class="fas fa-lock-open"></i>
        <p>Reset/Ganti PIN</p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('changePhone.index') }}">
        <i class="fas fa-phone"></i>
        <p>Ganti Nomor HP</p>
    </a>
</li>
<li class="nav-item">
    <a href="{{ route('blokir.index') }}">
        <i class="fas fa-exclamation-triangle"></i>
        <p>Blokir Akses</p>
    </a>
</li>

<li class="nav-item">
    <a data-toggle="collapse" href="#draft" class="collapsed" aria-expanded="false">
        <i class="fas fa-file-contract"></i>
        <p>Draft Proposal</p>
        <span class="caret"></span>
    </a>
    <div class="collapse" id="draft" style="">
        <ul class="nav nav-collapse">
            <li>
                <a href="{{ route('list.draft') }}">
                    <span class="sub-item">Draft Registrasi</span>
                </a>
            </li>
            <li>
                <a href="{{ route('changePin.drafts') }}">
                    <span class="sub-item">Draft Reset PIN</span>
                </a>
            </li>
            <li>
                <a href="{{ route('changePhone.drafts') }}">
                    <span class="sub-item">Draft Ganti No. HP</span>
                </a>
            </li>
            <li>
                <a href="{{ route('blokir.drafts') }}">
                    <span class="sub-item">Draft Blokir Akses</span>
                </a>
            </li>
            
        </ul>
    </div>
</li>