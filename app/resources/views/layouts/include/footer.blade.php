<footer class="footer">
    <div class="container-fluid">
        <div class="copyright ml-auto" style="color: #1572e8;">
            2020 <i class="fa fa-copyright"></i> PT. Bank Pembangunan Daerah Lampung | V.{{config('static.version_app')}}.{{config('static.type')}}
        </div>				
    </div>
</footer>