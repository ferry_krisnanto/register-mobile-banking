<div class="main-header">
    <!-- Logo Header -->
    <div class="logo-header" data-background-color="blue">

        <a href="#" class="logo text-white">
            {{config('app.name')}}
        </a>
        <button class="navbar-toggler sidenav-toggler ml-auto" type="button" data-toggle="collapse" data-target="collapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon">
                <i class="icon-menu"></i>
            </span>
        </button>
        <button class="topbar-toggler more"><i class="icon-options-vertical"></i></button>
        <div class="nav-toggle">
            <button class="btn btn-toggle toggle-sidebar">
                <i class="icon-menu"></i>
            </button>
        </div>
    </div>
    <!-- End Logo Header -->

    <!-- Navbar Header -->
    <nav class="navbar navbar-header navbar-expand-lg" data-background-color="blue">

        <div class="container-fluid">
            <ul class="navbar-nav topbar-nav ml-md-auto align-items-center">
                <li class="nav-item dropdown hidden-caret">
                    <a class="dropdown-toggle profile-pic" style="color: white;" data-toggle="dropdown" href="#" aria-expanded="false">
                        {{ auth()->user()->full_name }}
                    </a>
                    <span class="caret" style="color: white;"></span>
                    <ul class="dropdown-menu dropdown-user">
                        <div class="dropdown-user-scroll">
                            <li>
                                <div class="user-box">
                                    <div class="avatar-lg"><img src="{{ asset('img/user.png') }}" alt="image profile" class="avatar-img rounded"></div>
                                    <div class="u-text">
                                        <h4>{{ auth()->user()->username }}</h4>
                                        <p class="text-muted">{{ substr(auth()->user()->full_name, 0, 20) }}</p>
                                        <p class="text-muted">Kode Cabang: {{ auth()->user()->departement['code'] ?? '-' }}</p>
                                        <p class="text-muted">{{ auth()->user()->departement['name'] ?? '-' }}</p>
                                        <!-- <a href="" class="btn btn-xs btn-warning btn-sm">View Profile</a> -->
                                    </div>
                                </div>
                            </li>
                            <li>
                                <!-- <div class="dropdown-divider"></div> -->
                                <!-- <a class="dropdown-item reset" title="Reset Password" id="reset" href="">Reset Password</a> -->
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">Logout</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </div>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <!-- End Navbar -->
</div>
