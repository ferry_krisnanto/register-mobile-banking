<div class="sidebar sidebar-style-2">
    <div class="sidebar-wrapper scrollbar scrollbar-inner">
        <div class="sidebar-content">
            <div class="user">
                <div class="avatar-sm float-left mr-2">
                    <img src="{{ asset('img/user.png') }}" alt="..." class="avatar-img rounded-circle">
                </div>
                <div class="info">
                    <a aria-expanded="true">
                        <span style="color: black !important;">
                            {{ substr(Auth::user()->full_name,0,16) }}
                            <span><i class="fas fa-circle text-success"></i> Online</span>
                        </span>
                    </a>
                </div>
            </div>
            <ul class="nav nav-primary">
                @include('layouts.menu.index')
            </ul>
        </div>
    </div>
</div>
