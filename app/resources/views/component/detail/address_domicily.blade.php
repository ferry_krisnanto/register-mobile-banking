
<div class="form-group">
    <h3>Alamat Domisili</h3>
</div>
@if(!empty($data))
<div class="form-group">
    <label> Alamat</label>
    <input name="address" type="text" class="form-control" value="{{ $data->domicile_address->address ?? '' }}" id="address" disabled>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label >Propinsi</label>
            <input name="province_name" type="text" class="form-control" value="{{ $data->domicile_address->province_name ?? '' }}" id="province_name" disabled>
        </div>
        <div class="form-group">
            <label >Kecamatan</label>
            <input name="district_name" type="text" class="form-control" value="{{ $data->domicile_address->district_name ?? '' }}" id="district_name" disabled>
        </div>
        <div class="form-group">
            <label >RT</label>
            <input name="rt_number" type="text" class="form-control" value="{{ $data->domicile_address->rt_number ?? '' }}" id="address" disabled>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label >Kota / Kabupaten</label>
            <input name="city_name" type="text" class="form-control" value="{{ $data->domicile_address->city_name ?? '' }}" id="city_name" disabled>
        </div>
        <div class="form-group">
            <label >Kelurahan</label>
            <input name="sub_district_name" type="text" class="form-control" value="{{ $data->domicile_address->sub_district_name ?? '' }}" id="sub_district_name" disabled>
        </div>
        <div class="form-group">
            <label >RW</label>
            <input name="rw_number" type="text" class="form-control" value="{{ $data->domicile_address->rw_number ?? '' }}" id="address" disabled>
        </div>
    </div>
</div>
@endif