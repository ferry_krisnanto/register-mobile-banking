<div class="form-group">
    <h3>Alamat Sesuai Identitas</h3>
</div>
@if(!empty($data))
<div class="form-group">
    <label >Alamat Lengkap</label>
    <p class="form-control-static">{{$data->id_card_address->address ?? ''}}</p>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label >Propinsi</label>
                <p class="form-control-static">{{$data->id_card_address->province_name ?? ''}}</p>
            </div>
            <div class="form-group">
                <label >Kecamatan</label>
                <p class="form-control-static">{{$data->id_card_address->district_name ?? ''}}</p>
            </div>
            <div class="form-group">
                <label >RT / RW</label>
                <p class="form-control-static">{{$data->id_card_address->rt_number ?? ''}} / {{$data->id_card_address->rw_number ?? ''}}</p>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label >Kota / Kabupaten</label>
                <p class="form-control-static">{{$data->id_card_address->city_name ?? ''}}</p>
            </div>
            <div class="form-group">
                <label >Kelurahan</label>
                <p class="form-control-static">{{$data->id_card_address->sub_district_name ?? ''}}</p>
            </div>
        </div>
    </div>
</div>
@endif