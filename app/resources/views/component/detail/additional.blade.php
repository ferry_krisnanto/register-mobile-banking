<h3>Customer Detail</h3>
<hr>
@if(!empty($data))
<div class="form-group">
    <label> NIK Nasabah</label>
    <input type="text" class="form-control" value="{{ substr($data->customer->id_card_number, 0, 10) . 'xxxxxx' }}" id="nik" disabled>
</div>
<div class="form-group">
    <label> Nomor HP Nasabah</label>
    <p class="form-control-static">{{$data->phone_number}}</p>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label >No. Kartu Keluarga</label>
            <p class="form-control-static">{{ $data->customer->family_card_number ?? '-' }}</p>
        </div>
        <div class="form-group">
            <label >Nama Lengkap</label>
            <p class="form-control-static">{{$data->customer->full_name}}</p>
        </div>
        <div class="form-group">
            <label >Jenis Kelamin</label>
            <p class="form-control-static">{{$data->customer->gender}}</p>
        </div>
        <div class="form-group">
            <label >Pendidikan Terakhir</label>
            <p class="form-control-static">{{$data->customer->education}}</p>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label >Tanggal Lahir</label>
            <p class="form-control-static">{{$data->customer->birth_place}}, {{$data->customer->birth_date}}</p>
        </div>
        <div class="form-group">
            <label >Nama Ibu Kandung</label>
            <p class="form-control-static">{{$data->customer->mother_name}}</p>
        </div>
        <div class="form-group">
            <label >Agama</label>
            <p class="form-control-static">{{$data->customer->religion}}</p>
        </div>
        <div class="form-group">
            <label >Pekerjaan</label>
            <p class="form-control-static">{{$data->customer->job_type}}</p>
        </div>
    </div>
</div>
@endif