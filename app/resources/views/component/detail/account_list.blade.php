<div class="form-group">
    <h3>List Rekening Nasabah</h3>
</div>
@if(!empty($saving_accounts))
    @foreach($saving_accounts as $account)
    <label class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{ $account->product_name }}</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                <label>Name: {{$account->account_name}}</label><br>
                                <label>Status: {{$account->customer_status_name}}</label><br>
                                <label>Account Number: {{$account->account_number}}</label><br>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                    </div>

                </div>
            </div>
        </div>
        <span class="checkmark"></span>
    </label>
    @endforeach
@endif