@foreach($users as $user)
<label class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h4 class="card-title">{{ $user->full_name }}</h4>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                            <label>Identiti: {{$user->identity}}</label><br>
                            <label>Position: {{$user->position}}</label><br>
                            <label>Department: {{$user->department->name}}</label><br>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <br>
                        <input style="float:right; transform: scale(2.0);" name="account[]" type="checkbox" value="{{ $user->id }}">
                    </div>
                    <div class="col-md-1"></div>
                </div>
                
            </div>
        </div>
    </div>
    <span class="checkmark"></span>
</label>
@endforeach