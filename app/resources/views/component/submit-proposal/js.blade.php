<script>
    function approve(){
        var status = 'APPROVE';
        var text = 'Anda Yakin Data Sudah Benar, Dan Proposal Di Setujui ?';
        check(status, text);
    }

    function reject(){
        var status = 'REJECT';
        var text = 'Proposal Akan Di Reject ?';
        check(status, text);
    }

    function check(status, text){
        var checkbox = document.getElementById("checkbox");
        console.log(checkbox.checked);
        if (checkbox.checked == true) {
            actionButton(status, text);
        }else{
            swal({
                title: "Mohon Untuk DI Check List Terlebih Dahulu",
                allowOutsideClick: false
            });
        }
    }

    function actionButton(status, text){
        $("#btnSubmit").attr("disabled", true);
        swal({
			text: text,
			type: 'info',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
            confirmButtonText: 'Ya saya yakin',
            allowOutsideClick: false
		}).then((result) => {
			if (result.value) {
                swal({
                    title: "Mohon Tunggu!",
                    text: "Permintaan Sedang Di Prosess",
                    type: "success",
                    showConfirmButton: false,
                    allowOutsideClick: false
                });
                $("#btnSubmit").attr("disabled", true);

				$.ajax({
                    url: "{{ route( $routeName, $id ) }}",
					type: "POST",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {
                        'proposal_action': status,
                        'note': $('#catatan').val()
                    },
					success: function(response) {
                        console.log(response);
                        if(response.success){
                            swal({
                                type: 'success',
                                title: 'Success!',
                                text: 'Data berhasil disimpan!',
                                showCancelButton: false,
                                confirmButtonColor: '#1572e8',
                                confirmButtonText: 'Ok',
                                closeOnConfirm: true,
                                allowOutsideClick: false
                            }).then(function(){
                                window.location = "{{ route('home') }}";
                            });
                        } else{
                            swal({
                                title: "Gagal!",
                                text: response.message && response.message.indonesian,
                                type: "error",
                                allowOutsideClick: false
                            }).then(function(){
                                location.reload();
                            });
                            $("#btnSubmit").attr("disabled", false);
                        }
					},
					error: function(xhr) {
                        var res = xhr.responseJSON;
                        swal({
                            title: "Terjadi Kesalahan, Silahkan Ulangi Kembali!",
                            text: 'Pesan: '+res,
                            type: "error",
                            allowOutsideClick: false
                        });
                        $("#btnSubmit").attr("disabled", false);
					}
				});
			}else{
                $("#btnSubmit").attr("disabled", false);
            }
		});
    }
</script>