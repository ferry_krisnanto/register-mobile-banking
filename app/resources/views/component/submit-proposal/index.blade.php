@if($data->proposal_status != 'FINISHED' && isOtorisator())
<div class="row">
    <div class="col-md-6 ml-auto mr-auto">
        <div class="form-group">
            <label> Catatan</label>
            <input type="text" class="form-control" id="catatan" required>
        </div>
        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" id="checkbox" name="checkbox">
                <span class="form-check-sign" style="color:red;">Saya Dengan Sadar Menyetujui dan Bertanggung Jawab Penuh</span>
            </label>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3 ml-auto mr-auto">
        <button class="btn btn-success" type="button" onclick="approve()" id="btnSubmit" style="width:180px;">
            <span class="btn-label">
                <i class="fa fa-plus"></i>
            </span>
            Approve Account
        </button>
        <button class="btn btn-danger" type="button" onclick="reject()" style="width:180px;">
            <span class="btn-label">
                <i class="fa fa-plus"></i>
            </span>
            Reject Account
        </button>
    </div>
</div>
@endif