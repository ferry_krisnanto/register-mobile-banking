<nav>
    @php
    $totalPage = isset($paging->total_page) ? $paging->total_page : 1;
    @endphp
    <ul class="pagination justify-content-center">
        @if($page > 1)
        <li class="page-item">
            <a class="page-link" href="?page={{$page - 1}}">Previous</a>
        </li>
        @endif
        @if($page > 3)
        <li class="page-item">
            <a class="page-link" href="?page={{1}}">1</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="?page={{2}}">2</a>
        </li>
        <li class="page-item">
            <a class="page-link" >..</a>
        </li>
        @endif
        
        @for ($i = $page; $i < ($page + ($page < 4 ? 4 : 3)); $i++)
            @if($page == $i)
            <li class="page-item active"><a class="page-link" href="?page={{$i}}">{{$i}}</a></li>
            @elseif ($totalPage < $i)
            @else
            <li class="page-item"><a class="page-link" href="?page={{$i}}">{{$i}}</a></li>
            @endif
        @endfor	
        @if($page < $totalPage)		
        <li class="page-item">
            <a  class="page-link" href="?page={{$page + 1}}">Next</a>
        </li>
        @endif
    </ul>
</nav>