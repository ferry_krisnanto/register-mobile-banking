<!-- Modal -->
<div class="modal fade" id="show-pin" tabindex="-1" role="dialog" aria-labelledby="show-pin" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="show-pin">Pin Nasabah</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                PIN NASABAH:
            <br>
            <h1>{{ $newPin->default_credential ?? '-' }}</h1>
            <br>
                Masa Kadaluarsa Pin:
            <br>
            <b>{{ $newPin->default_credential_expired ?? '-' }}</b>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>