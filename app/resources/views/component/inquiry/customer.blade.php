<div class="col-md-12">
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="email2">NIK KTP</label>
                        <input type="email" class="form-control" id="nik" placeholder="Masukan NIK" required >
                        <small id="emailHelp2" class="form-text text-muted">
                        Masukan NIK KTP Nasabah
                        </small>
                    </div>

                    <div class="form-group">
                        <label for="email2">CIF Number</label>
                        <input type="email" class="form-control" id="cif" placeholder="Masukan NIK" required >
                        <small id="emailHelp2" class="form-text text-muted">
                        Masukan Nomor CIF Nasabah
                        </small>
                    </div>

                    <div class="form-group">
                        <label for="email2">Nomor HP</label>
                        <input type="email" class="form-control" id="phone" placeholder="Masukan NIK" required >
                        <small id="emailHelp2" class="form-text text-muted">
                        Nomor HP nasabah yang terdaftar di L Online (Mobile Banking).
                        </small>
                    </div>

                    <br>
                    <button type="submit" id="button-inquiry" class="btn btn-success" style="width: 100%;" >Cek Nasabah</button>
                </div>

                <div class="col-md-6">
                    <input type="hidden" value="" id="draftid">
                    <div id="add-form"></div>
                    <div id="div-reset" style="display:none;">
                        <button type="submit" id="buttonAction" class="btn btn-danger" style="width: 100%;" >{{$buttonName}}</button>
                    </div>
                    <div id="loading"></div>
                    <input type="hidden" value="" id="proposalid">
                    <!-- 711f483a-c0de-4525-a8bd-3d36d92d11dd -->
                </div>
            </div>
        </div>
    </div>
</div>