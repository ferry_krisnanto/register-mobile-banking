<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="input-group" id="inquiry-nik">
                                <input type="text" class="form-control" id="nik" name="nik" min="15" max="16" required placeholder="Masukan 16 Digit NIK">
                                <div class="input-group-prepend">
                                    <button class="btn btn-primary" type="button" id="button-inquiry-nik" >Inqury NIK</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>