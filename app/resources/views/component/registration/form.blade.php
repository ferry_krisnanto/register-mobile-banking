<div class="alert alert-success alert-block">
<button type="button" class="close" data-dismiss="alert">×</button>	
    <strong>Note: Untuk Update Data Alamat Nasabah Sesuai Tempat Tinggal Saat Ini</strong>
    <p>Default: Alamat saat ini yang ada di sistem Bank Lampung</p>
</div>
<hr>
<form method="POST" action="{{route('submit.draft')}}" id='form'>
@csrf
<input type="hidden" name="nik" id="nik_identity">
<div class="row">
    <div class="col-md-12">
    <div class="form-group">
            <label> NIK Nasabah</label>
            <input type="text" class="form-control" id="nik_nasabah" disabled>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <!-- <label >No. Kartu Keluarga</label> -->
                    <input name="family_card" type="hidden" value="-" class="form-control" id="family_card" required>
                </div>
                <div class="form-group">
                    <label >Nama Lengkap</label>
                    <input name="full_name" type="text" class="form-control" id="full_name" style="text-transform:uppercase" required>
                </div>
                <div class="form-group">
                    <label >Jenis Kelamin</label>
                    <select class="form-control" name="gender" id="gender" required>
                        <option value="MALE">Laki-Laki</option>
                        <option value="FEMALE">Perempuan</option>
                    </select>
                </div>
                <div class="form-group">
                    <label >Status Kawin</label>
                    <select class="form-control" name="marital_status" id="marital_status" required>
                        <option value=""> - Pilih - </option>
                        @foreach( config('static.marital') as $marital )
                        <option value="{{$marital}}">{{$marital}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label >Pendidikan Terakhir</label>
                    <select class="form-control" name="education" id="education" required>
                        <option value=""> - Pilih - </option>
                        @foreach( config('static.education') as $education )
                        <option value="{{$education}}">{{$education}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label >Tempat / Tanggal Lahir</label>
                    <input name="place_of_birth" type="text" class="form-control" id="place_of_birth" style="text-transform:uppercase" required>
                    <br>
                    <input name="date_of_birth" type="date" class="form-control" id="date_of_birth" required>
                </div>
                <div class="form-group">
                    <label >Nama Ibu Kandung</label>
                    <input name="mother_name" type="text" class="form-control" id="mother_name" style="text-transform:uppercase" required>
                </div>
                <div class="form-group">
                    <label >Agama</label>
                    <select class="form-control" name="religion" id="religion" required>
                        <option value=""> - Pilih - </option>
                        @foreach( config('static.religion') as $religon )
                        <option value="{{$religon}}">{{$religon}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label >Pekerjaan</label>
                    <select class="form-control" name="job" id="job" required>
                        <option > - Pilih -</option>
                        @foreach( config('static.profession') as $job )
                        <option value="{{$job}}">{{$job}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        <hr>

        <div class="form-group">
            <h3>Alamat KTP</h3>
        </div>
        <div class="form-group">
            <label> Alamat Lengkap</label>
            <input name="address" type="text" class="form-control" id="address" required>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Propinsi</label>
                    <input name="province_name" type="text" class="form-control" id="province_name" style="text-transform:uppercase" required>
                </div>
                <div class="form-group">
                    <label >Kecamatan</label>
                    <input name="district_name" type="text" class="form-control" id="district_name" style="text-transform:uppercase" required>
                </div>
                <div class="form-group">
                    <label >RT</label>
                    <input name="rt_number" type="number" class="form-control" id="rt_number" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label >Kota / Kabupaten</label>
                    <input name="city_name" type="text" class="form-control" id="city_name" style="text-transform:uppercase" required>
                </div>
                <div class="form-group">
                    <label >Kelurahan</label>
                    <input name="sub_district_name" type="text" class="form-control" id="sub_district_name" style="text-transform:uppercase" required>
                </div>
                <div class="form-group">
                    <label >RW</label>
                    <input name="rw_number" type="number" class="form-control" id="rw_number"required>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <hr>
        <div class="form-group">
            <h3>Alamat Domisili</h3>
        </div>

        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" id="checkbox" name="checkbox">
                <span class="form-check-sign">Sesuai Alamat KTP</span>
            </label>
        </div>

        <div id="address-domicily">
            <div class="form-group">
                <label> Alamat Lengkap</label>
                <input name="address_dom" type="text" class="form-control" id="address_dom" required>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label >Propinsi</label>
                        <input name="province_name_dom" type="text" class="form-control" id="province_name_dom" style="text-transform:uppercase" required>
                    </div>
                    <div class="form-group">
                        <label >Kecamatan</label>
                        <input name="district_name_dom" type="text" class="form-control" id="district_name_dom" style="text-transform:uppercase" required>
                    </div>
                    <div class="form-group">
                        <label >RT</label>
                        <input name="rt_number_dom" type="number" class="form-control" id="rt_number_dom" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label >Kota / Kabupaten</label>
                        <input name="city_name_dom" type="text" class="form-control" id="city_name_dom" style="text-transform:uppercase" required>
                    </div>
                    <div class="form-group">
                        <label >Kelurahan</label>
                        <input name="sub_district_name_dom" type="text" class="form-control" id="sub_district_name_dom" style="text-transform:uppercase" required>
                    </div>
                    <div class="form-group">
                        <label >RW</label>
                        <input name="rw_number_dom" type="number" class="form-control" id="rw_number_dom"required>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-2 ml-auto mr-auto">
        <button class="btn btn-primary" type="submit" id="button-save" hidden>
            <span class="btn-label">
                <i class="fa fa-plus"></i>
            </span>
            Simpan
        </button>
    </div>
</div>
</form>
