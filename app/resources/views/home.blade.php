@extends('layouts.admin')

@section('title')
    Home
@endsection

@push('css')
<style>
    .custom>tbody>tr>td, .custom>tbody>tr>th, .custom>thead>tr>td, .custom>thead>tr>th{
        padding: -3px !important;
    }
</style>
@endpush

@section('content')

@if ($message = Session::get('success'))
    <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
    </div>
@endif
@if ($message = Session::get('erorr'))
    <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
    </div>
@endif
<br><br>


<hr>
<div class="row">
    <div class="col-md-3">
        <div class="alert alert-primary">
            <strong>Menu Lampung Online</strong>
        </div>
    </div>
</div>
<div class="row">
    @if(isInitiator())
    <div class="col-sm-6 col-md-4">
        <a href="{{route('register.index')}}">
            <div class="card card-stats card-primary card-round">
                <div class="card-body">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="fas fa-mobile-alt"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <h4 class="card-title">Registrasi App</h4>
                                <p>Lampung Online</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4">
        <a href="{{route('changePin.index')}}">
            <div class="card card-stats card-info card-round">
                <div class="card-body">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="fas fa-lock-open"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <h4 class="card-title">Reset/Ganti PIN</h4>
                                <p>Lampung Online</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4">
        <a href="{{route('changePhone.index')}}">
            <div class="card card-stats card-warning card-round">
                <div class="card-body">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="fas fa-phone"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <h4 class="card-title">Ganti Nomor HP</h4>
                                <p>Lampung Online</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4">
        <a href="{{route('blokir.index')}}">
            <div class="card card-stats card-danger card-round">
                <div class="card-body">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="fas fa-exclamation-triangle"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <h4 class="card-title">Blokir Akses</h4>
                                <p>Lampung Online</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4">
        <a href="{{route('changeDevice.index')}}">
            <div class="card card-stats card-danger card-round">
                <div class="card-body">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="fas fa-exclamation-triangle"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <h4 class="card-title">Ganti Perangkat</h4>
                                <p>Lampung Online</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>

<hr>
<div class="row">
    <div class="col-md-3">
        <div class="alert alert-success">
            <strong>Menu Draft Proposal</strong>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6 col-md-4">
        <a href="{{route('list.draft')}}">
            <div class="card card-stats card-success card-round">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="fas fa-file-contract"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                            <p class="card-category">Draft</p>
                                <h4 class="card-title">Registrasi</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4">
        <a href="{{route('changePin.drafts')}}">
            <div class="card card-stats card-success card-round">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="fas fa-file-contract"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Draft</p>
                                <h4 class="card-title">Reset PIN</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4">
        <a href="{{route('changePhone.drafts')}}">
            <div class="card card-stats card-success card-round">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="fas fa-file-contract"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Draft</p>
                                <h4 class="card-title">Ganti No.HP</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4">
        <a href="{{route('blokir.drafts')}}">
            <div class="card card-stats card-success card-round">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="fas fa-file-contract"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Draft</p>
                                <h4 class="card-title">Blokir Akses</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4">
        <a href="{{route('changeDevice.drafts')}}">
            <div class="card card-stats card-success card-round">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="fas fa-file-contract"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Draft</p>
                                <h4 class="card-title">Ganti Perangkat</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    @endif
</div>

<hr>
<div class="row">
    <div class="col-md-3">
        <div class="alert alert-secondary">
            <strong>Menu Proposal Disetujui</strong>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-6 col-md-4">
        <a href="{{route('list.index')}}">
            <div class="card card-stats card-secondary card-round">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="fas fa-folder-open"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Proposal</p>
                                <h4 class="card-title">Registrasi</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4">
        <a href="{{route('changePin.proposals')}}">
            <div class="card card-stats card-secondary card-round">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="fas fa-folder-open"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Proposal</p>
                                <h4 class="card-title">Reset PIN</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4">
        <a href="{{route('changePhone.proposals')}}">
            <div class="card card-stats card-secondary card-round">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="fas fa-folder-open"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Proposal</p>
                                <h4 class="card-title">Ganti No. HP</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4">
        <a href="{{route('blokir.proposals')}}">
            <div class="card card-stats card-secondary card-round">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="fas fa-folder-open"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Proposal</p>
                                <h4 class="card-title">Blokir Akses</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-md-4">
        <a href="{{route('changeDevice.proposals')}}">
            <div class="card card-stats card-secondary card-round">
                <div class="card-body ">
                    <div class="row">
                        <div class="col-5">
                            <div class="icon-big text-center">
                                <i class="fas fa-folder-open"></i>
                            </div>
                        </div>
                        <div class="col-7 col-stats">
                            <div class="numbers">
                                <p class="card-category">Proposal</p>
                                <h4 class="card-title">Ganti Perangkat</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>

@endsection

@push('script')
<script src="{{ asset('assets/sweetalert2/sweetalert2.all.min.js') }}"></script>
@if ($message = Session::get('success'))
    <script>
        swal({
            title: "Success",
            text: "{{ $message }}",
            type: "success",
            allowOutsideClick: false,
        });
    </script>
@endif
@if ($message = Session::get('erorr'))
    <script>
        swal({
            title: "Success",
            text: "{{ $message }}",
            type: "error",
            allowOutsideClick: false,
        });
    </script>
@endif
@endpush