@extends('layouts.login')

@section('login')
<div class="container container-login animated fadeIn" style="padding: 50px 25px !important">
    <div class="container text-center">
        <img src="{{ asset('img/logo-bl.png') }}" width="70%" alt="">
        <br>
        <a href="#" class="btn btn-primary btn-sm text-center mt-3">{{config('app.name')}}</a>
    </div>

    @if ($message = Session::get('failed'))
    <br>
    <div class="alert alert-danger alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>	
		<strong>{{ $message }}</strong>
	</div>
    @endif

    <form action="{{ route('login') }}" method="POST">
        @csrf
        @method('POST')
        <div class="login-form" style="margin-top: 0px !important;">
            <div class="form-group">
                <label for="username" class="placeholder"><b>Username</b></label>
                <input id="username" name="username" type="text" class="form-control" required autofocus>
            </div>
            <div class="form-group">
                <label for="password" class="placeholder"><b>Password</b></label>
                <div class="position-relative">
                    <input id="password" name="password" type="password" class="form-control" required>
                    <div class="show-password">
                        <i class="icon-eye"></i>
                    </div>
                </div>
            </div>
            <div class="form-group form-action-d-flex mb-3">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" class="custom-control-input" id="rememberme">
                    <label class="custom-control-label m-0" for="rememberme">Remember Me</label>
                </div>
                <button type="submit" class="btn btn-primary col-md-4 float-right mt-3 mt-sm-0">Login</button>
            </div>
        </div>
    </form>
</div>
@endsection
