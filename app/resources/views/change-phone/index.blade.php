@extends('layouts.admin')

@section('title')
    Step 1 - Ganti Nomor HP Nasabah
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="email2">NIK KTP</label>
                            <input type="text" class="form-control" id="nik" placeholder="Masukan NIK">
                            <small class="form-text text-muted">
                            Masukan NIK KTP Nasabah
                            </small>
                        </div>

                        <div class="form-group">
                            <label for="email2">CIF Number</label>
                            <input type="text" class="form-control" id="cif" placeholder="Masukan CIF">
                            <small class="form-text text-muted">
                            Masukan Nomor CIF Nasabah
                            </small>
                        </div>

                        <div class="form-group">
                            <label for="email2">Nomor HP Sebelumnya (Lama)</label>
                            <input type="email" class="form-control" id="phone" placeholder="Masukan No HP" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*?)\..*/g, '$1');">
                            <small class="form-text text-muted">
                            Nomor HP nasabah yang terdaftar di L Online (Mobile Banking).
                            </small>
                        </div>

                        <br>
                        <button type="submit" id="button-inquiry" class="btn btn-success" style="width: 100%;" >Cek Nasabah</button>
                    </div>

                    <div class="col-md-6">
                        <input type="hidden" value="" id="customerId">
                        <div id="add-form"></div>
                        <div id="div-reset" style="display:none;">
                            <button class="btn btn-danger" style="float:right;width:100%;" data-toggle="modal" data-target="#change-phone" id="btnSubmit">Ganti No. HP Nasabah</button>
                        </div>
                        <div id="loading"></div>
                        <input type="hidden" value="" id="proposalid">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="change-phone" tabindex="-1" role="dialog" aria-labelledby="show-pin" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content container">
            <div class="modal-header">
                <h5 class="modal-title" id="show-pin">Phone Nasabah</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="form-group">
                <label for="email2">Nomor HP Baru Nasabah</label>
                <input type="text" class="form-control" id="newPhone" placeholder="Masukan No HP: 0812xxxxx " oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*?)\..*/g, '$1');">
                <small class="form-text text-muted">
                Nomor HP Baru Nasabah yang akan di daftarkan di L Online (Mobile Banking).
                </small>
            </div>
            
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="button-action">Ganti Nomor HP</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<!-- Sweetalert2 -->
<script src="{{ asset('assets/sweetalert2/sweetalert2.all.min.js') }}"></script>
<script>
    var resetDiv = document.getElementById("div-reset");
    $("#button-inquiry").click(function(){
        var nik = $("#nik").val();
        var cif = $("#cif").val();
        var phone = $("#phone").val();

        if (nik=="" || cif=="" || phone=="") {
            swal(
                'Form Kosong',
                'Data NIK, CIF, No HP tidak boleh kosong',
                'error'
            )
        }else{
            swal({
                title: "Mohon Tunggu",
                text: "Permintaan Sedang Di Proses",
                allowOutsideClick: false
            });
            getInquiry();
        }
    });

    $("#button-action").click(function(){
        var customerId = $("#customerId").val();
        var newPhone = $("#newPhone").val();
        if (customerId) {
            swal({
                title: "Mohon Tunggu!",
                text: "Permintaan Sedang Di Prosess",
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
            loading()
            resetDiv.style.display = "none";
            sendDraft(customerId, newPhone)

        }else{
            swal({
                title: "Gagal",
                text: "Customer ID Kosong, Mohon Inquiry Ulang",
                type: "success",
                allowOutsideClick: false
            });
            resetDiv.style.display = "block";
        }
    });

    function getInquiry() {
        $.ajax({
            url: "{{ route('changePhone.inquiry') }}",
            type:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'nik': $('#nik').val(),
                'cif': $('#cif').val(),
                'phone': $('#phone').val(),
            },
            success: function(response){
                // console.log(response)
                if(response.code == 200){
                    swal(
                        'Response: '+response.code,
                        'Data Berhasil Di Temukan',
                        'success'
                    );
                    document.getElementById("customerId").value = response.data.id;
                    resetDiv.style.display = "block";
                    addForm(response);
                }
                else{
                    swal(
                        'Response: '+response.code,
                        response.message && response.message.indonesian,
                        'error'
                    )
                }
            },
            error: function (response) {
                // console.log(response);
                swal(
                    'Terjadi Error: Silahkan Hubungi IT',
                    response,
                    'error'
                )
            }
        });
    }
    function loading(){
        $("#loading").html(`
            <br>
            <div class="row">
                <div class="col-md-6 ml-auto mr-auto">
                    <div>Sedang Proccess, Mohon Tunggu</div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 ml-auto mr-auto">
                    <div class="loader"></div>
                </div>
            </div>
        `);
    }

    function addForm(response){
        $("#add-form").html(`
            <div class="form-group">
                <label >Nama Lengkap</label>
                <input type="text" class="form-control" disabled="" value="${response.data.full_name}">
            </div>
            <div class="form-group">
                <label >Tempat Tanggal Lahir</label>
                <input type="text" class="form-control" disabled="" value="${response.data.birth_place}">
                <input type="text" class="form-control" disabled="" value="${response.data.birth_date}">
            </div>
            <div class="form-group">
                <label >Jenis Kelamin</label>
                <input type="text" class="form-control" disabled="" value="${response.data.gender}">
            </div>
            <div class="form-group">
                <label >Nama Ibu Kandung</label>
                <input type="text" class="form-control" disabled="" value="${response.data.mother_name}">
            </div>
            <div class="form-group">
                <label >Alamat KTP</label>
                <input type="text" class="form-control" disabled="" value="${response.data.id_card_address.address}">
            </div>
            <div class="form-group">
                <label >Alamat Domisili</label>
                <input type="text" class="form-control" disabled="" value="${response.data.domicile_address.address}">
            </div>
        `);
    }

    function sendDraft(customerId, newPhone){
        $("#loading").html("Berhasil, Silahkan Tunggu Sedang Loading");
        $.ajax({
            url: "{{ route('changePhone.proposal') }}",
            type:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'customerId': customerId,
                'newPhone': newPhone,
            },
            success: function(response){
                console.log(response);
                if(response.success){
                    if(response.otp.success){
                        window.location = response.url;
                    }else{
                        swal(
                            'Gagal',
                            response.otp.message.indonesian,
                            'error'
                        )
                    }
                }else{
                    swal(
                        'Gagal',
                        response.message && response.message.indonesian,
                        'error'
                    )
                }
            },
            error: function (response) {
                // console.log(response);
                swal.close();
                swal(
                    'Terjadi Error: Koneksi Terputus',
                    response,
                    'error'
                )
                resetDiv.style.display = "block";
                $("#loading").html("Something Wrong, Silahkan Ulangi Kembali");
            }
        });
    }
</script>
@endpush