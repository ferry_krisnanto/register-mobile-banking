@extends('layouts.admin')

@section('title')
    Step 3 - Pilih Otorisator Ganti Nomor HP Nasabah
@endsection

@section('content')
<div>
    <div class="card">
        <div class="card-header">
            <h4>Silahkan Pilih Atasan/SVP/Pimpinan Cabang Sebagai Penyetuju</h4>
        </div>
    </div>

    <form action="{{route('changePhone.submit', $draftId)}}" method="POST">
        @csrf
        <input type="hidden" name="draft_id" value="{{$draftId}}">
        @include('component.otorisator', [
            'users' => $users
        ])
        <br>
        <button class="btn btn-primary">Submit</button>
    </form>
</div>
@endsection