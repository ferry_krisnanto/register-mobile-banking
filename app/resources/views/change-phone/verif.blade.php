@extends('layouts.admin')

@section('title')
    Step 2 - Ganti Nomor HP Nasabah
@endsection

@section('content')
<div>
    <div class="card">
        <div class="card-header">
            <h4>Verifikasi Nomor HP Baru Nasabah</h4>
        </div>
    </div>
    <div class="card-body">
        <input type="hidden" id="draft_id"  value="{{$draftId}}">
        <input type="hidden" id="new_phone"  value="{{$newPhone}}">
        <div class="form-group">
            <label >Kode OTP</label>
            <input type="email" class="form-control" id="otp_code" placeholder="Masukan 6 Digit Kode OTP">
            <small class="form-text text-muted">
            Masukan Kode OTP yang Di Kirimkan Oleh Nasabah
            </small>
        </div>
        <button type="submit" id="button-verif" class="btn btn-success" >Verifikasi OTP</button>
        <button type="submit" id="button-resend" class="btn btn-danger" >Kirimkan Ulang OTP</button>
        <button hidden type="submit" id="button-waiting" class="btn btn-danger" >Kirimkan Ulang (60 Detik)</button>
    </div>
</div>
@endsection

@push('script')
<!-- Sweetalert2 -->
<script src="{{ asset('assets/sweetalert2/sweetalert2.all.min.js') }}"></script>
<script>
    $("#button-verif").click(function(){
        var otp_code = $("#otp_code").val();
        var draft_id = $("#draft_id").val();

        if (otp_code=="" || draft_id=="") {
            swal(
                'Form Kosong',
                'Data NIK, CIF, No HP tidak boleh kosong',
                'error'
            )
        }else{
            swal({
                title: "Mohon Tunggu!",
                text: "Permintaan Sedang Di Prosess",
                type: "success",
                showConfirmButton: false,
                allowOutsideClick: false
            });
            verifOtp();
        }
    });

    function verifOtp(){
        $.ajax({
            url: "{{ route('changePhone.verify') }}",
            type:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'otp': $('#otp_code').val(),
                'draftId': $('#draft_id').val()
            },
            success: function(response){
                // console.log(response)
                if(response.callback.code == 200){
                    if(response.callback.data.is_valid){
                        swal({
                            title: "Berhasil!",
                            text: "Nomor HP Berhasil Di Verifikasi, Mohon Tunggu Sedang Di Prosess",
                            type: "success",
                            showConfirmButton: false,
                            allowOutsideClick: false
                        });
                        window.location = response.url;
                    }else{
                        swal({
                            title: "Gagal!",
                            text: response.callback.data.reason,
                            type: "error",
                            allowOutsideClick: false
                        });
                    }
                }
                else{
                    swal(
                        'Response: '+response.code,
                        response.message && response.message.indonesian,
                        'error'
                    )
                }
            },
            error: function (response) {
                // console.log(response);
                swal(
                    'Terjadi Error: Silahkan Hubungi IT',
                    response,
                    'error'
                )
            }
        });
    }

    $("#button-resend").click(function(){
        document.getElementById("button-resend").hidden = true;
        document.getElementById("button-waiting").hidden = false;
        waitingButton();

        $.ajax({
            url: "{{ route('changePhone.resendOtp') }}",
            type:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'newPhone': $('#new_phone').val(),
                'draftId': $('#draft_id').val()
            },
            success: function(response){
                // console.log(response)
                if(response.code == 200){
                    swal({
                        title: "Berhasil Dikirimkan!",
                        text: response.data.reason,
                        type: "success",
                        allowOutsideClick: false
                    });
                }
                else{
                    swal(
                        'Response: '+response.code,
                        response.message && response.message.indonesian,
                        'error'
                    )
                }
            },
            error: function (response) {
                // console.log(response);
                swal(
                    'Terjadi Error: Silahkan Hubungi IT',
                    response,
                    'error'
                )
            }
        });
    });

    function waitingButton(){
        var timeleft = 60;
        var downloadTimer = setInterval(function(){
        timeleft--;
        var text = `Kirim Ulang OTP (Tersedia dalam ${timeleft} Detik)`;
        document.getElementById("button-waiting").textContent = text;
            if(timeleft <= 0){
                clearInterval(downloadTimer);
                document.getElementById("button-resend").hidden = false;
                document.getElementById("button-waiting").hidden = true;
            }
        },1000);
    }
</script>
@endpush