@extends('layouts.admin')

@section('title')
    Step 2 - Get Account
@endsection

@push('css')
<style>
    .custom>tbody>tr>td, .custom>tbody>tr>th, .custom>thead>tr>td, .custom>thead>tr>th{
        padding: -3px !important;
    }
</style>
<style>
    .loader {
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite; /* Safari */
    animation: spin 2s linear infinite;
    }

    /* Safari */
    @-webkit-keyframes spin {
    0% { -webkit-transform: rotate(0deg); }
    100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
    }
</style>
@endpush

@section('content')
@if ($message = Session::get('failed'))
    <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
    </div>
@endif
@if(!empty($data))
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >Nama Lengkap</label>
                            <p class="form-control-static">{{$data['name'] ?? ''}}</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label >NIK</label>
                            <p class="form-control-static">{{$data['nik'] ?? ''}}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-success alert-block">
                            <button type="button" class="close" data-dismiss="alert">×</button>	
                            <strong>Note: Masukan Nomor CIF Nasabah, Pastikan </strong>
                            <p>1: CIF BENAR</p>
                            <p>2: REKENING SESUAI</p>
                            <p>3: PILIH SALAH SATU REKENING, Atau BISA BEBERAPA REKENING SEKALIGUS yang ingin DI AKTIFKAN</p>
                            <p>-: Apabila Terjadi Kesalahan Sistem (Error), Mohon untuk di Foto dan Hubungi : <b><u>DIV TI BANK LAMPUNG</u></b></p>
                        </div>
                        <div class="form-group">
                            <div class="input-group" id="inquiry-nik">
                                <input type="text" class="form-control" id="cif" name="cif" required>
                                <div class="input-group-prepend">
                                    <button class="btn btn-primary" type="button" id="button-cif" placeholder="Masukan CIF">Mencari CIF</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" id="form"></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('script')
<script>
    $("#button-cif").click(function(){
        getCif();
    });

    function getCif() {
        $("#form").html(`
            <div class="row">
                <div class="col-md-3 ml-auto mr-auto">
                    <div class="loader"></div>
                </div>
            </div>
        `);
        $.ajax({
            url: "{{ route('account.cif') }}",
            type:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'cif': $('#cif').val(),
                'draft_id': "{{$draftId}}",
            },
            success: function(response){
                $("#form").html(response);
            },
            error: function (response) {
                console.log(response);
                $("#form").html("Something Wrong");
            }
        });
    }
</script>
@endpush