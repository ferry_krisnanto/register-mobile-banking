@extends('layouts.admin')

@section('title')
    Step 3 - Pilih Otorisator
@endsection

@push('css')
<style>
    .custom>tbody>tr>td, .custom>tbody>tr>th, .custom>thead>tr>td, .custom>thead>tr>th{
        padding: -3px !important;
    }
</style>
<style>
    .loader {
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite; /* Safari */
    animation: spin 2s linear infinite;
    }

    /* Safari */
    @-webkit-keyframes spin {
    0% { -webkit-transform: rotate(0deg); }
    100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
    }
</style>
@endpush

@section('content')
@if ($message = Session::get('failed'))
    <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>{{ $message }}</strong>
    </div>
@endif

<div>
<div class="card">
    <div class="card-header">
        <h4>Silahkan Pilih Atasan/SVP/Pimpinan Cabang Sebagai Penyetuju</h4>
    </div>
</div>

<form action="{{route('register.final', $draftId)}}" method="post">
    @csrf
    <input type="hidden" name="draft_id" value="{{$draftId}}">
    @foreach($users as $user)
    <label class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{ $user->full_name }}</h4>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                <label>Identiti: {{$user->identity}}</label><br>
                                <label>Position: {{$user->position}}</label><br>
                                <label>Department: {{$user->department->name}}</label><br>
                            </div>
                        </div>
                        <div class="col-md-5">
                            <br>
                            <input style="float:right; transform: scale(2.0);" name="account[]" type="checkbox" value="{{ $user->id }}">
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    
                </div>
            </div>
        </div>
        <span class="checkmark"></span>
    </label>
    @endforeach
    <br>
    <button class="btn btn-primary">Submit</button>
</form>
</div>

@endsection

@push('script')
@endpush


<!-- <br>
<div class="alert alert-success alert-block">
<button type="button" class="close" data-dismiss="alert">×</button>	
    <strong>Pesan: Nomor HP Sudah Terverifikasi</strong>
    
</div>
<div class="col-md-2 ml-auto mr-auto">
    <a href="{{route('register.final', $draftId)}}" class="btn btn-primary">Lanjutkan</a>
</div> -->