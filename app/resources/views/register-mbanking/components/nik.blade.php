<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>	
        <strong>Data KTP Nasabah Di Temukan</strong>
        
    </div>
</div>
<hr>
<div class="row">
    <!-- S: Identitas -->
    <div class="col-md-12">
        <div class="form-group">
            <label> NIK Nasabah</label>
            <input type="text" class="form-control" value="{{$data->id_card}}" id="nik" disabled>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >No. Kartu Keluarga</label>
                    <p class="form-control-static">{{$data->family_card ?? ''}}</p>
                </div>
                <div class="form-group">
                    <label >Nama Lengkap</label>
                    <p class="form-control-static">{{$data->full_name ?? ''}}</p>
                </div>
                <div class="form-group">
                    <label >Jenis Kelamin</label>
                    <p class="form-control-static">{{$data->gender ?? ''}}</p>
                </div>
                <div class="form-group">
                    <label >Pendidikan Terakhir</label>
                    <p class="form-control-static">{{$data->education ?? ''}}</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label >Tanggal Lahir</label>
                    <p class="form-control-static">{{$data->place_of_birth ?? ''}}, {{$data->date_of_birth ?? ''}}</p>
                </div>
                <div class="form-group">
                    <label >Nama Ibu Kandung</label>
                    <p class="form-control-static">{{$data->mother_name ?? ''}}</p>
                </div>
                <div class="form-group">
                    <label >Agama</label>
                    <p class="form-control-static">{{$data->religion ?? ''}}</p>
                </div>
                <div class="form-group">
                    <label >Pekerjaan</label>
                    <p class="form-control-static">{{$data->job ?? ''}}</p>
                </div>
            </div>
        </div>
        <hr>
        <div class="form-group">
            <label >Alamat Lengkap - Sesuai KTP</label>
            <p class="form-control-static">{{$data->address_detail->address ?? ''}}</p>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label >Propinsi</label>
                        <p class="form-control-static">{{$data->address_detail->province_name ?? ''}}</p>
                    </div>
                    <div class="form-group">
                        <label >Kecamatan</label>
                        <p class="form-control-static">{{$data->address_detail->sub_district_name ?? ''}}</p>
                    </div>
                    <div class="form-group">
                        <label >RT / RW</label>
                        <p class="form-control-static">{{$data->address_detail->hamlet_number ?? ''}} / {{$data->address_detail->neighbourhood_number ?? ''}}</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label >Kota / Kabupaten</label>
                        <p class="form-control-static">{{$data->address_detail->district_name ?? ''}}</p>
                    </div>
                    <div class="form-group">
                        <label >Kelurahan</label>
                        <p class="form-control-static">{{$data->address_detail->urban_village_name ?? ''}}</p>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-3 ml-auto mr-auto">
        <button class="btn btn-primary" type="button" id="button-inquiry-address">
            <span class="btn-label">
                <i class="fa fa-plus"></i>
            </span>
            Lanjutkan Aktivasi Akun
        </button>
    </div>
</div>

<script>
    $("#button-inquiry-address").click(function(){
        getInquiryAddress();
    });
    function getInquiryAddress() {
        $("#form-register").html(`
            <div class="row">
                <div class="col-md-3 ml-auto mr-auto">
                    <div class="loader"></div>
                </div>
            </div>
        `);
        $.ajax({
            url: "{{ route('register.draft') }}",
            type:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'nik': $('#nik').val(),
            },
            success: function(response){
                // $("#inquiry-nik").html('');
                $("#form-register").html(response);
            },
            error: function (response) {
                console.log(response);
                $("#form-register").html("Something Wrong");
            }
        });
    }
</script>
