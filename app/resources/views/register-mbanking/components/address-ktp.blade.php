<div class="alert alert-success alert-block">
<button type="button" class="close" data-dismiss="alert">×</button>	
    <strong>Note: Tanyakan ke Nasabah Apakah Data Alamat Nasabah Sudah Sesuai Dengan Tempat Tinggal Saat Ini</strong>
    <p>Default: Alamat saat ini sesuai data KTP</p>
</div>
<hr>
<form method="POST" action="{{route('submit.draft')}}">
@csrf
<input type="hidden" name="nik" value="{{$nik}}">
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <h3>Alamat Domisili</h3>
        </div>
        <div class="form-group">
            <label> Alamat</label>
            <input name="address" type="text" class="form-control" value="{{ $data->identity['address_detail']['address'] ?? '' }}" id="address" required>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Propinsi</label>
                    <input name="province_name" type="text" class="form-control" value="{{ $data->identity['address_detail']['province_name'] ?? '' }}" id="province_name" required>
                </div>
                <div class="form-group">
                    <label >Kecamatan</label>
                    <input name="district_name" type="text" class="form-control" value="{{ $data->identity['address_detail']['sub_district_name'] ?? '' }}" id="district_name" required>
                </div>
                <div class="form-group">
                    <label >RT</label>
                    <input name="rt_number" type="text" class="form-control" value="{{ $data->identity['address_detail']['hamlet_number'] ?? '' }}" id="address" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label >Kota / Kabupaten</label>
                    <input name="city_name" type="text" class="form-control" value="{{ $data->identity['address_detail']['district_name'] ?? '' }}" id="city_name" required>
                </div>
                <div class="form-group">
                    <label >Kelurahan</label>
                    <input name="sub_district_name" type="text" class="form-control" value="{{ $data->identity['address_detail']['urban_village_name'] ?? '' }}" id="sub_district_name" required>
                </div>
                <div class="form-group">
                    <label >RW</label>
                    <input name="rw_number" type="text" class="form-control" value="{{ $data->identity['address_detail']['neighbourhood_number'] ?? '' }}" id="address"required>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="row">
    <div class="col-md-2 ml-auto mr-auto">
        <button class="btn btn-primary" type="submit">
            <span class="btn-label">
                <i class="fa fa-plus"></i>
            </span>
            Simpan
        </button>
    </div>
</div>
</form>