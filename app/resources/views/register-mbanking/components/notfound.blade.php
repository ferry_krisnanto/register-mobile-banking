<div class="row">
    <div class="col-md-6 ml-auto mr-auto">
        <div class="card card-stats card-round">
            <div class="card-body">
                <div class="row">
                    <div class="col-5">
                        <div class="icon-big text-center">
                            <i class="flaticon-error text-danger"></i>
                        </div>
                    </div>
                    <div class="col-7 col-stats">
                        <div class="numbers">
                            <p class="card-category">Errors:</p>
                            <h4 class="card-title">{{$data}}</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>