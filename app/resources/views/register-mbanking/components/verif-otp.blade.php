

<script>
    $("#button-verif-otp").click(function(){
        verifOtp();
        console.log("tes");
    });

    function verifOtp() {
        $("#verify").html(`
            <div class="row">
                <div class="col-md-3 ml-auto mr-auto">
                    <div class="loader"></div>
                </div>
            </div>
        `);
        $.ajax({
            url: "{{ route('verif.otp') }}",
            type:'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'otp_code': $('#otp-code').val(),
                'draft_id': "{{$draftId}}",
            },
            success: function(response){
                $("#verify").html(response);
                
            },
            error: function (response) {
                console.log(response);
                $("#verify").html("Something Wrong");
            }
        });
    }
</script>