<div class="alert alert-success alert-block">
<button type="button" class="close" data-dismiss="alert">×</button>	
    <strong>Note: Untuk Update Data Alamat Nasabah Sesuai Tempat Tinggal Saat Ini</strong>
    <p>Default: Alamat saat ini yang ada di sistem Bank Lampung</p>
</div>
<hr>
<form method="POST" action="{{route('submit.draft')}}">
@csrf
<input type="hidden" name="nik" value="{{$identity}}">
<div class="row">
    <div class="col-md-12">
    <div class="form-group">
            <label> NIK Nasabah</label>
            <input type="text" class="form-control" value="{{$identity}}" id="nik" disabled>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >No. Kartu Keluarga</label>
                    <input name="family_card" type="text" class="form-control" id="family_card" required>
                </div>
                <div class="form-group">
                    <label >Nama Lengkap</label>
                    <input name="full_name" type="text" class="form-control" id="full_name" required>
                </div>
                <div class="form-group">
                    <label >Jenis Kelamin</label>
                    <select class="form-control" name="gender" required>
                        <option value="MALE" 
                        @if($data->gender == 'MALE')
                            selected="selected"
                        @endif
                        >Laki-Laki</option>
                        <option value="FEMALE"
                        @if($data->gender == 'FEMALE')
                            selected="selected"
                        @endif
                        >Perempuan</option>
                    </select>
                </div>
                <div class="form-group">
                    <label >Status Kawin</label>
                    <!-- <input name="gender" type="text" class="form-control" id="gender" required> -->
                    <select class="form-control" name="marital_status" required>
                        <option value=""> - Pilih - </option>
                        <option value="KAWIN">KAWIN</option>
                        <option value="BELUM KAWIN">BELUM KAWIN</option>
                        <option value="CERAI MATI">CERAI MATI</option>
                        <option value="CERAI HIDUP">BELUM HIDUP</option>
                    </select>
                </div>
                <div class="form-group">
                    <label >Pendidikan Terakhir</label>
                    <select class="form-control" name="education" required>
                        <option value=""> - Pilih - </option>
                        <option value="TDK/ BLM SKLH">TIDAK/BELUM SEKOLAH</option>
                        <option value="TAMAT SD/ SDRJT">SD / SEDERAJAT</option>
                        <option value="SLTP/ SDRJT">SMP / SLTP SEDERAJAT</option>
                        <option value="SLTA/ SDRJT">SMA / SLTA SEDERAJAT</option>
                        <option value="D-I/II">D1 / D2</option>
                        <option value="BUDHA">Sarjana / S1</option>
                        <option value="KONGHUCU">Magister / S2</option>
                        <option value="KONGHUCU">Doktor / S3</option>
                        <option value="Lainnya">Lainnya</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label >Tempat / Tanggal Lahir</label>
                    <input name="place_of_birth" type="text" class="form-control" id="place_of_birth" required>
                    <br>
                    <input name="date_of_birth" type="date" class="form-control" id="date_of_birth" value="{{$data->birth_date}}" required>
                </div>
                <div class="form-group">
                    <label >Nama Ibu Kandung</label>
                    <input name="mother_name" type="text" class="form-control" id="mother_name" required>
                </div>
                <div class="form-group">
                    <label >Agama</label>
                    <select class="form-control" name="religion" required>
                        <option value=""> - Pilih - </option>
                        <option value="ISLAM">ISLAM</option>
                        <option value="KRISTEN">KRISTEN</option>
                        <option value="KATHOLIK">KATHOLIK</option>
                        <option value="HINDU">HINDU</option>
                        <option value="BUDHA">BUDHA</option>
                        <option value="KONGHUCU">KONGHUCU</option>
                    </select>
                </div>
                <div class="form-group">
                    <label >Pekerjaan</label>
                    <select class="form-control" name="job" required>
                        <option > - Pilih -</option>
                        <option value="BELUM/TIDAK BEKERJA">TIDAK/BELUM BEKERJA</option>
                        <option value="MENGURUS RUMAH TANGGA">MENGURUS RUMAH TANGGA</option>
                        <option value="PELAJAR/MAHASISWA">PELAJAR/MAHASISWA</option>
                        <option value="PNS/ASN">PNS/ASN</option>
                        <option value="TNI/POLRI">TNI/POLRI</option>
                        <option value="PENSIUNAN">PENSIUNAN</option>
                        <option value="KARYAWAN SWASTA">KARYAWAN SWASTA</option>
                        <option value="KARYAWAN BUMD/BUMN">KARYAWAN BUMD/BUMN</option>
                        <option value="TENAGA MEDIS">TENAGA MEDIS</option>
                        <option value="WIRASWASTA">WIRASWASTA</option>
                        <option value="GURU/DOSEN">GURU/DOSEN</option>
                        <option value="LAINNYA">LAINNYA</option>
                    </select>
                </div>
            </div>
        </div>
        <hr>

        <div class="form-group">
            <h3>Alamat KTP</h3>
        </div>
        <div class="form-group">
            <label> Alamat Lengkap</label>
            <input name="address" type="text" class="form-control" id="address" required>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label >Propinsi</label>
                    <input name="province_name" type="text" class="form-control" id="province_name" required>
                </div>
                <div class="form-group">
                    <label >Kecamatan</label>
                    <input name="district_name" type="text" class="form-control" id="district_name" required>
                </div>
                <div class="form-group">
                    <label >RT</label>
                    <input name="rt_number" type="text" class="form-control" id="rt_number" required>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label >Kota / Kabupaten</label>
                    <input name="city_name" type="text" class="form-control" id="city_name" required>
                </div>
                <div class="form-group">
                    <label >Kelurahan</label>
                    <input name="sub_district_name" type="text" class="form-control" id="sub_district_name" required>
                </div>
                <div class="form-group">
                    <label >RW</label>
                    <input name="rw_number" type="text" class="form-control" id="rw_number"required>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <hr>
        <div class="form-group">
            <h3>Alamat Domisili</h3>
        </div>

        <div class="form-check">
            <label class="form-check-label">
                <input class="form-check-input" type="checkbox" id="checkbox" name="checkbox">
                <span class="form-check-sign">Sesuai Alamat KTP</span>
            </label>
        </div>

        <div id="address-domicily">
            <div class="form-group">
                <label> Alamat Lengkap</label>
                <input name="address_dom" type="text" class="form-control" id="address_dom" required>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label >Propinsi</label>
                        <input name="province_name_dom" type="text" class="form-control" id="province_name_dom" required>
                    </div>
                    <div class="form-group">
                        <label >Kecamatan</label>
                        <input name="district_name_dom" type="text" class="form-control" id="district_name_dom" required>
                    </div>
                    <div class="form-group">
                        <label >RT</label>
                        <input name="rt_number_dom" type="text" class="form-control" id="rt_number_dom" required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label >Kota / Kabupaten</label>
                        <input name="city_name_dom" type="text" class="form-control" id="city_name_dom" required>
                    </div>
                    <div class="form-group">
                        <label >Kelurahan</label>
                        <input name="sub_district_name_dom" type="text" class="form-control" id="sub_district_name_dom" required>
                    </div>
                    <div class="form-group">
                        <label >RW</label>
                        <input name="rw_number_dom" type="text" class="form-control" id="rw_number_dom"required>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-2 ml-auto mr-auto">
        <button class="btn btn-primary" type="submit">
            <span class="btn-label">
                <i class="fa fa-plus"></i>
            </span>
            Simpan
        </button>
    </div>
</div>
</form>
<script>
    $('#checkbox').change(function() {
        if(this.checked) {
            $('#address-domicily').hide();
            
            $("#address_dom").removeAttr('required');
            $("#province_name_dom").removeAttr('required');
            $("#district_name_dom").removeAttr('required');
            $("#rt_number_dom").removeAttr('required');
            $("#city_name_dom").removeAttr('required');
            $("#sub_district_name_dom").removeAttr('required');
            $("#rw_number_dom").removeAttr('required');
        }else{
            $('#address-domicily').show();

            $("#address_dom").attr('required', '');
            $("#province_name_dom").attr('required', '');
            $("#district_name_dom").attr('required', '');
            $("#rt_number_dom").attr('required', '');
            $("#city_name_dom").attr('required', '');
            $("#sub_district_name_dom").attr('required', '');
            $("#rw_number_dom").attr('required', '');
        }      
    });
</script>