@extends('layouts.admin')

@section('title')
    List View
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">

                @if($data->proposal_status == 'FINISHED')
                <b>Status: </b>
                <span class="btn-sm btn-success">FINISH</span>
                @include('component.show-pin.button')
                @endif
            </div>
            <div class="card-body">
                @include('component.detail.additional', ['data' => $data->additional_data ?? []])
                <hr>
                @include('component.detail.address_identity', ['data' => $data->additional_data ?? []])
                <hr>
                @include('component.detail.address_domicily', ['data' => $data->additional_data ?? []])
                <hr>
                @include('component.detail.account_list', ['saving_accounts' => $data->additional_data->saving_accounts ?? []])
            </div>
            @include('component.submit-proposal.index')
        </div>
    </div>
</div>
@include('component.show-pin.modals', ['newPin' => $newPin])
@endsection

@push('script')
    <!-- Sweetalert2 -->
    <script src="{{ asset('assets/sweetalert2/sweetalert2.all.min.js') }}"></script>
    @include('component.submit-proposal.js', [
        'id' => $id,
        'routeName' => 'otorisator.aprovalProposal'
    ])
@endpush
