<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Services\Api\Punic;

class OtorisatorController extends Controller
{
    public function __construct(Punic $punic)
    {
        $this->punic  = $punic;
    }

    public function aprovalProposal(Request $request, $id)
    {
        $proposal_action = $request->proposal_action;
        $note = $request->note;
        $leng = strlen($note);
        if( $leng <= 5 ){
            return response()->json([
                'success'   => false,
                'app'       => 'PUNIC FE',
                'message'   => [
                    'indonesian' => 'Catatan tidak boleh kosong, dan minimal terdiri dari 5 Karakter'
                ]
            ]);
        }
        
        $body = [
            "proposal_action"   => $proposal_action,
            "note"  => $note
        ];
        $response = $this->punic->v1("proposals/$id/flow", 'POST', $body);
        return response()->json($response);
    }
}