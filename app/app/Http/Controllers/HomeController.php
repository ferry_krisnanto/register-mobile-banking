<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Services\Connection\Response;

class HomeController extends Controller
{
    public function __construct(Response $response)
    {
        // $this->middleware('auth');
        $this->res = $response;
    }

    public function index(Request $request)
    {
        return view('home');
    }

    public function mockInquiry(Request $request)
    {
        $nik = $request->id_card;
        return $this->mock($nik);
    }

    public function mock($nik)
    {
        $statusCode = 200;
        $message = 'success';

        $data1 = [
            "address_detail" => [
               "address" => "ARUM LESTARI PERMAI BLOK A NO 2 KARIMUN JAWA", 
               "province_id" => 18, 
               "province_name" => "LAMPUNG", 
               "district_id" => 71, 
               "district_name" => "KOTA BANDAR LAMPUNG", 
               "sub_district_id" => 2, 
               "sub_district_name" => "SUKARAME", 
               "urban_village_id" => 1003, 
               "urban_village_name" => "SUKARAME", 
               "hamlet_number" => 0, 
               "neighbourhood_number" => 3 
            ], 
            "id_card" => "1871020202960002", 
            "family_card" => "1871020603070009", 
            "full_name" => "FIRMANSYAH", 
            "religion" => "ISLAM", 
            "job" => "BELUM/TIDAK BEKERJA", 
            "education" => "SLTA/SEDERAJAT", 
            "date_of_birth" => "02-02-1996", 
            "place_of_birth" => "SURABAYA", 
            "marital_status" => "BELUM KAWIN", 
            "blood_group" => "TIDAK TAHU", 
            "gender" => "Laki-Laki", 
            "mother_name" => "DJAMILAH" 
        ];

        $data2 = [
            "address_detail" => [
               "address" => "ARUM LESTARI PERMAI BLOK A NO 2 KARIMUN JAWA", 
               "province_id" => 18, 
               "province_name" => "LAMPUNG", 
               "district_id" => 71, 
               "district_name" => "KOTA BANDAR LAMPUNG", 
               "sub_district_id" => 2, 
               "sub_district_name" => "SUKARAME", 
               "urban_village_id" => 1003, 
               "urban_village_name" => "SUKARAME", 
               "hamlet_number" => 0, 
               "neighbourhood_number" => 3 
            ], 
            "id_card" => "1871020202960003", 
            "family_card" => "1871020603070009", 
            "full_name" => "FERI KRISNANTO", 
            "religion" => "ISLAM", 
            "job" => "BELUM/TIDAK BEKERJA", 
            "education" => "SLTA/SEDERAJAT", 
            "date_of_birth" => "02-02-1996", 
            "place_of_birth" => "SURABAYA", 
            "marital_status" => "BELUM KAWIN", 
            "blood_group" => "TIDAK TAHU", 
            "gender" => "Laki-Laki", 
            "mother_name" => "DJAMILAH" 
        ];

        if($nik == '1871020202960002'){
            $data = $data1;
        }elseif ($nik == '1871020202960003') {
            $data = $data2;
        }else{
            $data = [];
        }
        
        $response = $this->res->success($data, $message, $statusCode);
        return $response;
    }

    public function getToken()
    {
        return json_encode(Auth::user());
    }
}