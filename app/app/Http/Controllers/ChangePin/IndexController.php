<?php

namespace App\Http\Controllers\ChangePin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

use App\Services\Api\Punic;

class IndexController extends Controller
{
    public function __construct(Punic $punic)
    {
        $this->punic  = $punic;
    }

    public function index()
    {
        return view('change-pin.index');
    }

    public function inquiry(Request $request)
    {
        $nik    = $request->nik;
        $cif    = $request->cif;
        $phoneNumber = $request->phone;

        $response = $this->punic->v1("customers?phone_number=$phoneNumber&nik=$nik&cif=$cif");
        return response()->json($response);
    }

    public function proposal(Request $request)
    {
        $id     = $request->id;
        $body   = [
            "draft_type"    => "CC_INIT",
            "application"   => "RESET_CREDENTIAL",
            "additional_data"   => [
                "customer_id"   => $id
            ]
        ];
        $response   = $this->punic->v2("proposals", "POST", $body);
        $proposalId = $response->data->draft_id;
        $url = route('changePin.otorisator', $proposalId);
        return response()->json([
            'url'   => $url,
            'id'    => $proposalId
        ]);
    }

    public function otorisator($id)
    {
        $draftId = $id;
        $response = $this->punic->requestOtorisator();
        $users = $response->data->content;
        return view('change-pin.otorisator', compact('draftId', 'users'));
    }

    public function submit(Request $request, $id)
    {
        $accountSelected = $request->account;
        if(empty($accountSelected)){
            return redirect()->back()->with(['failed' => "Harus Pilih Salah Satu"]);
        }

        $otorisator = $this->punic->submitOtorisator($accountSelected);
        $response   = $this->punic->v1("proposals/$id", $method = 'POST', $otorisator);
        if($response->success){
            return redirect()->route('home')->with(['success' => 'Ganti Pin Nasabah Berhasil, Silakan Minta Pimpinan Cabang Untuk Approve']);
        }else{
            return redirect()->route('home')->with(['erorr' => json_encode($response)]);
        }
    }

    public function listDraft(Request $request)
    {
        $page = empty($request->page) ? 1 : $request->page;
        $application = 'RESET_CREDENTIAL';
        $doc_type    = 'DRAFT';

        // response
        $response = $this->punic->listProposals($page, $doc_type, $application);
        $datas = $response->data ?? [];
        $paging = $response->paging ?? [];
        return view('change-pin.proposal', compact('datas', 'page', 'paging'));
    }

    public function listProposal(Request $request)
    {
        $page = empty($request->page) ? 1 : $request->page;
        $application = 'RESET_CREDENTIAL';
        $doc_type    = 'PROPOSAL';

        // response
        $response = $this->punic->listProposals($page, $doc_type, $application);
        $datas = $response->data ?? [];
        $paging = $response->paging ?? [];
        return view('change-pin.proposal', compact('datas', 'page', 'paging'));
    }

    public function detail($id)
    {
        $response = $this->punic->v1("proposals/$id");
        $reqPin = $this->punic->pinDefault($id);

        $data = $response->data;
        // return json_encode($data);
        $newPin = isset($reqPin->data) ? $reqPin->data : [];

        return view('change-pin.detail', compact('data', 'newPin', 'id'));
    }

}