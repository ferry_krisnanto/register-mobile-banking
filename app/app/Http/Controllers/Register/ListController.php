<?php

namespace App\Http\Controllers\Register;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use App\Services\Api\Punic;

class ListController extends Controller
{   
    public function __construct(Punic $punic)
    {
        $this->punic  = $punic;
        $this->application = 'REGISTRATION_MOBILE_BANKING';
    }

    public function listDraft(Request $request)
    {
        $page = empty($request->page) ? 1 : $request->page;
        $application = $this->application;
        $doc_type    = 'DRAFT';

        // response
        $response = $this->punic->listProposals($page, $doc_type, $application);
        $datas = $response->data ?? [];
        $paging = $response->paging ?? [];
        return view('registration.proposal', compact('datas', 'page', 'paging'));
    }

    public function listProposal(Request $request)
    {
        $page = empty($request->page) ? 1 : $request->page;
        $application = $this->application;
        $doc_type    = 'PROPOSAL';

        // response
        $response = $this->punic->listProposals($page, $doc_type, $application);
        $datas = $response->data ?? [];
        $paging = $response->paging ?? [];
        return view('registration.proposal', compact('datas', 'page', 'paging'));
    }

    public function detail($id)
    {
        $response = $this->punic->v2("proposals/$id");
        $reqPin = $this->punic->pinDefault($id);

        $data = $response->data;
        $newPin = isset($reqPin->data) ? $reqPin->data : [];

        return view('register-mbanking.detail', compact('data', 'newPin', 'id'));
    }
    
    public function checkStep($draftId)
    {
        $path = "proposals/$draftId";
        $response = $this->punic->v1($path);

        if(isset($response->data->saving_accounts) && count($response->data->saving_accounts) > 0){
            return redirect()->route('Register.otorisator', $draftId);
        }
        if(isset($response->data->phone_number)){
            return redirect()->route('RegisterInquiry.step3', $draftId);
        }
        return redirect()->route('RegisterInquiry.step2', $draftId);
    }
}