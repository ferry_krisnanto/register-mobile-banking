<?php

namespace App\Http\Controllers\Register;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use App\Services\Api\Punic;
use App\Services\Api\Endpoint;
use App\Services\Connection\Response;

class InquiryController extends Controller{

    public function __construct(Response $response, Punic $punic)
    {
        $this->response = $response;
        $this->punic    = $punic;
    }

    public function index()
    {
        return view('registration.index');
    }

    public function step2($id)
    {
        $draftId = $id;
        return view('registration.otp', compact('draftId'));
    }

    public function step3($id)
    {
        $draftId = $id;
        return view('registration.account', compact('draftId'));
    }

    public function inquiry(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nik' => 'required|min:16|max:16',
        ]);
        if ($validator->fails()) {
            $message = 'The nik must be at least 16 characters';
            return $response = $this->response->errorResponse($message, 400);
        }

        $identity = $request->nik;
        $path ="identities/parse/$identity";
        // $path ="identities/inquiry/$identity";
        $areca = Endpoint::gateway('areca/v1');
        $response = $areca->getWithAuth($path);
        
        return response()->json($response);
    }

    public function validateOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'otp_code' => 'required|min:6|max:6',
            'draft_id' => 'required',
        ]);
        if ($validator->fails()) {
            $errors = $validator->errors();
            $message = json_encode($errors);
            return $response = $this->response->errorResponse($message, 400);
        }

        $otpCode = $request->otp_code;
        $draftId = $request->draft_id;

        $path = "customers/otp?otp=$otpCode&draft_id=$draftId";   
        $response = $this->punic->v1($path);
        return response()->json($response); 
    }

    public function otorisator($id)
    {
        $draftId = $id;
        $response = $this->punic->requestOtorisator();
        $users = $response->data->content;
        return view('registration.otorisator', compact('draftId', 'users'));
    }

    public function submit(Request $request, $id)
    {
        $accountSelected = $request->account;
        if(empty($accountSelected)){
            return redirect()->back()->with(['failed' => "Harus Pilih Salah Satu"]);
        }

        $otorisator = $this->punic->submitOtorisator($accountSelected);
        $response   = $this->punic->v1("proposals/$id", $method = 'POST', $otorisator);

        if($response->success){
            return redirect()->route('home')->with(['success' => 'Registrasi Nasabah Berhasil, Silakan Minta Pimpinan Cabang Untuk Approve']);
        }else{
            return redirect()->route('home')->with(['erorr' => json_encode($response)]);
        }
    }
}