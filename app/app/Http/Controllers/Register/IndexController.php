<?php

namespace App\Http\Controllers\Register;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;

use Auth;
use App\Services\Date\Date;
use App\Services\Connection\Outbound;
use App\Services\Connection\Signature;
use App\Services\Connection\Response;
use App\Http\Controllers\HomeController;

use App\Models\Customer;

use App\Services\Api\Endpoint;
use App\Services\Api\Punic;

class IndexController extends Controller
{
    private $dt;
    private $out;
    private $signatur;

    public function __construct(Punic $punic, Date $date, Outbound $outbound, Signature $signature, Response $response, HomeController $mock)
    {
        $this->middleware(['auth', 'initiator']);
        $this->dt   = $date;
        $this->out  = $outbound;
        $this->res  = $response;
        $this->signature = $signature;
        $this->mock = $mock;

        $this->punic = $punic;
    }

    // Start: Draft
    public function submitDraft(Request $request)
    {
        $storeIdentity = [
            "draft_type" => "IDENTITY",
            "id_card_number" => $request->nik,
            "application" => "REGISTRATION_MOBILE_BANKING",
            "family_card_number" => $request->family_card,
            "full_name" => $request->full_name,
            "religion" => $request->religion,
            "job_type" => $request->job,
            "education" => $request->education,
            "birth_place" => $request->place_of_birth,
            "birth_date" => $request->date_of_birth,
            "marital_status" => $request->marital_status,
            "gender" => $request->gender,
            "father_name" => '-',
            "mother_name" => $request->mother_name,
            "id_card_address" => [
                "address" => $request->address,
                "province_id" => 0,
                "province_name" => $request->province_name,
                "city_id" => 0,
                "city_name" => $request->city_name,
                "district_Id" => 0,
                "district_name" => $request->district_name,
                "sub_district_id" => 0,
                "sub_district_name" => $request->sub_district_name, 
                "rt_number" => $request->rt_number,
                "rw_number" => $request->rw_number
            ]
        ];

        $response = $this->postDraft($storeIdentity);
        $response = $this->convert($response);
        if ($response->code != 200) {
            return view('registration.error', compact('response'));
        }
        if (isset($response->data->draft_id)) {
            $draftId = $response->data->draft_id;
        }else{
            return view('registration.error', compact('response'));
        }
        if($request->checkbox == 'on'){
            $storeAddress = [
                "draft_type" => "DOMICILE_ADDRESS",
                "draft_id" => $draftId,
                "domicile_address" =>  [
                    "address" => $request->address,
                    "province_name" => $request->province_name,
                    "city_name" => $request->city_name,
                    "district_name" => $request->district_name,
                    "sub_district_name" => $request->sub_district_name,
                    "rt_number" => $request->rt_number,
                    "rw_number" => $request->rw_number
                ]
            ];
        }else{
            $storeAddress = [
                "draft_type" => "DOMICILE_ADDRESS",
                "draft_id" => $draftId,
                "domicile_address" =>  [
                    "address" => $request->address_dom,
                    "province_name" => $request->province_name_dom,
                    "city_name" => $request->city_name_dom,
                    "district_name" => $request->district_name_dom,
                    "sub_district_name" => $request->sub_district_name_dom,
                    "rt_number" => $request->rt_number_dom,
                    "rw_number" => $request->rw_number_dom
                ]
            ];
        }
        $response = $this->postDraft($storeAddress);
        $response = $this->convert($response);
        if ($response->code != 200 || $response->code != '200') {
            return view('registration.error', compact('response'));
        }
        return redirect()->route('RegisterInquiry.step2', $draftId);
    }

    

    // Start: Account
    public function getAccountByCif(Request $request)
    {
        $cif        = $request->cif;
        $account_number = $request->account_number;
        $draftId    = $request->draft_id;


        $listAllAccount = $this->inquiryAllAccount($cif);
        if($listAllAccount->code !=200){
            return $this->errorResponse($listAllAccount);
        }

        $collection = collect($listAllAccount->data->saving_accounts ?? []);
        $check = $collection->where('account_number', $account_number);
        if(count($check) < 1){
            // $response= "CIF dan No Rekening Tidak Sesuai, Mohon Diperiksa Kembali";
            $message = (object) [
                'indonesian' => "CIF dan No Rekening Tidak Sesuai, Mohon Diperiksa Kembali"
            ];
            $response =  (object) [
                'code' => '404',
                'error_code' => 'Not Found',
                'message' => $message,
            ];
            return $this->errorResponse($response);
        }

        $hasRegisteredAccount = $this->hasRegisteredAccount($cif);
        return view('registration.account-list', compact(
            'listAllAccount', 'hasRegisteredAccount', 'draftId'
        ));
    }
    private function inquiryAllAccount($cif)
    {
        $path = "customers/$cif";
        $punic = Endpoint::punic('ficus');
        $response = $punic->getWithAuth($path);
        return $this->convert($response);
    }
    public function hasRegisteredAccount($cif)
    {
        $path = "customers/saving-accounts/$cif";
        $punic = Endpoint::punic('punic');
        $response = $punic->getWithAuth($path);
        $datas = $this->convert($response);
        if( isset($datas->data)){
            $hasRegisteredAccount = $datas->data;
            $collection = collect($hasRegisteredAccount);
            $newCollection = $collection->map(function ($item, $key){
                return $item->account_number;
            });
            return $newCollection->toArray();
        }else{
            return [];
        }
    }
    public function saveAccount(Request $request)
    {
        $draftId    = $request->draft_id;
        $cif        = $request->customer_branch .''.$request->customer_number;
        $arrAccount = $request->account;
        if(empty($arrAccount) || empty($cif)){ 
            return redirect()->back()->with(['failed' => "Tidak Boleh Kosong, Harap Ceklist Minimal 1 Rekening"]);
        }

        $allAccount = $this->inquiryAllAccount($cif);
        if(isset($allAccount->data->saving_accounts)){
            $allAccount = $allAccount->data->saving_accounts;
        }
        else{
            return redirect()->back()->with(['failed' => "Punic FE :: Failed Parse Data Account"]);
        }
        $newAccount = collect($allAccount);
        $filterAccount = $newAccount->whereIn('account_number', $arrAccount)->values();
        
        $body = [
            "draft_type" => "ACCOUNTS",
            "draft_id" => $draftId,
            "cif" => $cif,
            "saving_accounts" =>  $filterAccount
        ];
        $response = $this->postDraft($body);
        if ($response->code != 200) {
            $message = '';
            if(isset($response->message->indonesian)){
                $message = $response->message->indonesian;
            }else{
                $message = json_encode($response);
            }
            return redirect()->back()->with(['failed' => "Punic FE :: ".$message]);
        }else{
            return redirect()->route('Register.otorisator', $draftId);
        }
    }
    // End: Account

    private function postDraft($body)
    {
        $path = "proposals";
        $method = 'POST';
        $response = $this->punic->v1($path, $method, $body);
        return $this->convert($response);
    }
    public function convert($response)
    {
        if(is_object($response)){
            return $response;
        }else{
            return $response = json_decode($response);
        }
    }
    private function errorResponse($response)
    {
        try {
            return '
                <div class="card card-stats card-round">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-5">
                                <div class="icon-big text-center">
                                    <i class="flaticon-error text-danger"></i>
                                </div>
                            </div>
                            <div class="col-7 col-stats">
                                <div class="numbers">
                                    <p class="card-category">Errors: '.$response->code.'</p>
                                    <h4 class="card-title">'.$response->message->indonesian.'</h4>
                                    <h2 class="card-title">'.$response->error_code.'</h2>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            ';
        } catch (\Throwable $th) {
            return json_encode($response);
        }   
    }
    // End: 















    public function index()
    {
        return view('register-mbanking.inquiry-nik');
    }

    public function inquryNik(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nik' => 'required|min:15|max:16',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return view('register-mbanking.components.error', compact('errors'));
        }

        // return $nik = $request->nik;
        // $method = 'GET';
        // $token = Auth::user()->token;
        // $url = "/areca/inquiry?id_card=$nik&force=false";

        // $header = [
        //     'Accept: application/json',
        //     "Authorization" => "Bearer $token",
        //     "Content-Type" => "application/json"
        // ];

        // $body = [];
        // $timeStamp = $this->dt->dateRfc3339();
        // $token = Auth::user()->token;
        // $newBody = json_encode($body);
        // $signature = $this->signature->get($method, $url, $token, $newBody, $timeStamp);
        // $header = $this->out->setHeader($token, $timeStamp, $signature);

        // $response = $this->out->sendRequest($method, $url, $header, []);
        // $response = $this->convert($response);

        // try {
        //     $data = $response->data;
        // } catch (\Throwable $th) {
        //     return json_encode($response);
        // }

        // if (empty($data)) {
        //     $data = 'NIK: Tidak Dapat Di Temukan';
        //     return view('register-mbanking.components.notfound', compact('data'));
        // }else{
            $nik = $request->nik;
            $data = (object) $data = [
                'id_card' => $nik
            ];
            
            $customer = Customer::updateOrInsert([
                'nik' => $nik
            ], [
                'nik'       => $nik,
                'identity'  => $data
            ]);
            // return view('register-mbanking.components.nik', compact('data'));
            return view('register-mbanking.components.kyc-manual', compact('data', 'nik'));
        // }
    }

    

    private function inquryAddressByNik($request)
    {
        $validator = Validator::make($request->all(), [
            'nik' => 'required|min:15|max:16',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return view('register-mbanking.components.error', compact('errors'));
        }

        $method = 'GET';
        $url = "/punic/customers/address/$request->nik";
        $body = "";
        $timeStamp = $this->dt->dateRfc3339();
        $token = Auth::user()->token;
        $signature = $this->signature->get($method, $url, $token, $body, $timeStamp);
        $header = $this->out->setHeader($token, $timeStamp, $signature);
        $response = $this->out->sendRequest($method, $url, $header);

        $response = $this->convert($response);
        if ($response->code != (200 || 404 )) {
            return $response->message->indonesian;
        }

        $data = $response->data ?? [];
        $nik  = $request->nik;

        if (empty($data)) {
            $customer = Customer::updateOrInsert([
                'nik' => $nik
            ], [
                'nik' => $nik,
            ]);

            $data = Customer::where('nik', $nik)->first();
            return view('register-mbanking.components.address-ktp', compact('data', 'nik'));
        }else{
            $customer = Customer::updateOrInsert([
                'nik' => $nik
            ], [
                'nik'       => $nik,
                'address'   => $data
            ]);
            return view('register-mbanking.components.address-domicily', compact('data', 'nik'));
        }

    }

    public function createDraft(Request $request)
    {
        $step = 1;
        if ($step == 1) {
            return $this->inquryAddressByNik($request);
        }elseif($step = 2){
            return "<div>Home</div>";
        }
    }

    

    public function registerAccount($id)
    {
        $draftId = $id;
        $data = [];

        // Handling IF ACCOUNT HAS SUBMIT
        $method = 'GET';
        $token = Auth::user()->token;
        $header = [
            'Accept: application/json',
            "Authorization" => "Bearer $token",
            "Content-Type" => "application/json"
        ];
        $url = "punic/proposals/$id";
        $response = $this->out->sendRequest($method, $url, $header, []);
        $response = $this->convert($response);

        if(isset($response->data->saving_accounts) && count($response->data->saving_accounts) > 0){
            return redirect()->route('choose.otorisator', $draftId);
        }

        return view('register-mbanking.get-account', compact('draftId', 'data'));
    }

    public function registerAccountContinue($id, $name, $nik)
    {
        $draftId = $id;
        $data = [
            'nik' => $nik,
            'name'=> $name
        ];

        // Handling IF NO Handphone HAS SUBMIT
        $method = 'GET';
        $token = Auth::user()->token;
        $header = [
            'Accept: application/json',
            "Authorization" => "Bearer $token",
            "Content-Type" => "application/json"
        ];
        $url = "punic/proposals/$id";
        $response = $this->out->sendRequest($method, $url, $header, []);
        $response = $this->convert($response);

        if(isset($response->data->saving_accounts) && count($response->data->saving_accounts) > 0){
            return redirect()->route('choose.otorisator', $draftId);
        }

        if(isset($response->data->phone_number)){
            return redirect()->route('register.account', $draftId);
        }

        return view('register-mbanking.send-otp', compact('draftId', 'data'));
    }

    

    public function inquiryCustomerByCif($cif)
    {
        $method = 'GET';
        $url = "/ficus/customers/$cif";
        $body = "";
        $timeStamp = $this->dt->dateRfc3339();
        $token = Auth::user()->token;
        $signature = $this->signature->get($method, $url, $token, $body, $timeStamp);
        $header = $this->out->setHeader($token, $timeStamp, $signature);

        return $request = $this->out->sendRequest($method, $url, $header);
    }

    public function inquiryCustomerByCifCompare($cif)
    {
        $method = 'GET';
        $url = "/punic/customers/saving-accounts/$cif";
        $body = "";
        $timeStamp = $this->dt->dateRfc3339();
        $token = Auth::user()->token;
        $signature = $this->signature->get($method, $url, $token, $body, $timeStamp);
        $header = $this->out->setHeader($token, $timeStamp, $signature);

        return $request = $this->out->sendRequest($method, $url, $header);
    }

    public function submitAccount(Request $request)
    {
        $draftId    = $request->draft_id;
        $arrAccount = $request->account;

        if(empty($arrAccount)){
            return redirect()->back()->with(['failed' => "Tidak Boleh Kosong"]);
        }

        $data = Customer::select('account', 'cif')->where('draft_id', $draftId)->first();
        $account = collect($data->account)->whereIn('account_number', $arrAccount)->values();

        $body = [
            "draft_type" => "ACCOUNTS",
            "draft_id" => $draftId,
            "cif" => $data->cif,
            "saving_accounts" =>  $account
        ];
        $response = $this->postDraft($body);
        $response = $this->convert($response);

        if ($response->code != 200) {
            return redirect()->back()->with(['failed' => json_encode($response)]);
        }
        return redirect()->route('choose.otorisator', $draftId);
    }

    public function sendOtp($id)
    {
        $draftId = $id;
        return view('register-mbanking.send-otp', compact('draftId'));
    }

    public function apiSendOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'phone' => 'required|min:10|max:13',
            'draft_id' => 'required',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return view('register-mbanking.components.error', compact('errors'));
        }

        $draftId = $request->draft_id;
        $phone = $request->phone;
        // return view('register-mbanking.components.verif-otp', compact('draftId'));

        $cif = Customer::select('draft_id')->where('draft_id', $draftId)->first();

        $body = [
            "draft_id" => $draftId,
            "phone_number" => $phone
        ];

        $method = 'POST';
        $url = "/punic/customers/otp";
        $timeStamp = $this->dt->dateRfc3339();
        $token = Auth::user()->token;
        $newBody = json_encode($body);
        $signature = $this->signature->get($method, $url, $token, $newBody, $timeStamp);
        $header = $this->out->setHeader($token, $timeStamp, $signature);

        $response = $this->out->sendRequest($method, $url, $header, $body);
        $response = $this->convert($response);
        if ($response->code != 200) {
            return json_encode($response);
        }
        return view('register-mbanking.components.verif-otp', compact('draftId'));
    }

    public function apiVerifOtp(Request $request)
    {
        $draftId = $request->draft_id;
        $code = $request->otp_code;

        $body = [];
        $method = 'GET';
        $url = "/punic/customers/otp?otp=$code&draft_id=$draftId";
        $timeStamp = $this->dt->dateRfc3339();
        $token = Auth::user()->token;
        $newBody = json_encode($body);
        $signature = $this->signature->get($method, $url, $token, $newBody, $timeStamp);
        $header = $this->out->setHeader($token, $timeStamp, $signature);

        $response = $this->out->sendRequest($method, $url, $header, $body);
        if ($response->data->is_valid == false) {
            return "OTP Tidak Sesuai";
        }
        return view('register-mbanking.components.success-otp', compact('draftId'));
    }

    public function chooseOtorisator($id)
    {
        $draftId = $id;
        $token = Auth::user()->token;
        $depCode = Auth::user()->departement['code'];
        // $depCode = 380;

        $method = 'GET';
        $url = "auth/employes?page=0&size=20&search=roleApp;PUNIC AND departCode;$depCode AND role;OTORISATOR";
        $header = [
            'Accept: application/json',
            "Authorization" => "Bearer $token",
            "Content-Type" => "application/json"
        ];

        $response = $this->out->sendRequest($method, $url, $header, []);
        $newResponse = $this->convert($response);
        
        if(isset($newResponse->data->content)){
            $users = $newResponse->data->content;
        }else{
            return response()->json($newResponse);
            return view('register-mbanking.choose-otorisator', compact('draftId', 'users'));
        }
        
        $update = Customer::where('draft_id', $draftId)->update([
            'otoristors' => $users
        ]);

        // return json_encode($users);
        return view('register-mbanking.choose-otorisator', compact('draftId', 'users'));
    }

    public function registerFinal(Request $request, $draftId)
    {

        $getReqOtorisator = $request->account;
        if(empty($getReqOtorisator)){
            return redirect()->back()->with(['failed' => "Harus Pilih Salah Satu"]);
        }

        // return $getReqOtorisator;
        $newOtoristor = [];
        $get = Customer::select('otoristors')->where('draft_id', $draftId)->first();
        foreach ($get->otoristors as $index => $user) {

            if (in_array($user['id'], $getReqOtorisator)) {
                $newOtoristor[$index] = [
                    "username" => $user['user']['username'],
                    "full_name" => $user['full_name'],
                    "position" => $user['position'],
                    "department" => [
                        "code" => $user['department']['code'],
                        "name" => $user['department']['name'],
                        "type" => $user['department']['type']
                    ]
                ];
            }
        }

        if(empty($newOtoristor)){
            return redirect()->back()->with(['failed' => "Terjadi Kesalahan, Array Kosong"]);
        }
        // return json_encode($newOtoristor);
        // die;
        $body = [
            'otorisators' => $newOtoristor
        ];

        $method = 'POST';
        $url = "/punic/proposals/$draftId";
        $timeStamp = $this->dt->dateRfc3339();
        $token = Auth::user()->token;
        $newBody = json_encode($body);
        $signature = $this->signature->get($method, $url, $token, $newBody, $timeStamp);
        $header = $this->out->setHeader($token, $timeStamp, $signature);

        $response = $this->out->sendRequest($method, $url, $header, $body);
        $response = $this->convert($response);

        if($response->code != 200){
            return redirect()->back()->with(['failed' => $response->message->indonesian]);
        }else{
            Customer::where('draft_id', $draftId)->delete();
            return redirect()->route('home')->with(['success' => 'Berhasil Registrasi']);
        }
    }

    

    public function mock($nik)
    {
        $data = [
            "address_detail" => [
               "address" => "ARUM LESTARI PERMAI BLOK A NO 2 KARIMUN JAWA",
               "province_id" => 18,
               "province_name" => "LAMPUNG",
               "district_id" => 71,
               "district_name" => "KOTA BANDAR LAMPUNG",
               "sub_district_id" => 2,
               "sub_district_name" => "SUKARAME",
               "urban_village_id" => 1003,
               "urban_village_name" => "SUKARAME",
               "hamlet_number" => 0,
               "neighbourhood_number" => 3
            ],
            "id_card" => "1871020202960002",
            "family_card" => "1871020603070009",
            "full_name" => "FIRMANSYAH",
            "religion" => "ISLAM",
            "job" => "BELUM/TIDAK BEKERJA",
            "education" => "SLTA/SEDERAJAT",
            "date_of_birth" => "02-02-1996",
            "place_of_birth" => "SURABAYA",
            "marital_status" => "BELUM KAWIN",
            "blood_group" => "TIDAK TAHU",
            "gender" => "Laki-Laki",
            "mother_name" => "DJAMILAH"
        ];

        $data2 = [
            "address_detail" => [
               "address" => "ARUM LESTARI PERMAI BLOK A NO 2 KARIMUN JAWA",
               "province_id" => 18,
               "province_name" => "LAMPUNG",
               "district_id" => 71,
               "district_name" => "KOTA BANDAR LAMPUNG",
               "sub_district_id" => 2,
               "sub_district_name" => "SUKARAME",
               "urban_village_id" => 1003,
               "urban_village_name" => "SUKARAME",
               "hamlet_number" => 0,
               "neighbourhood_number" => 3
            ],
            "id_card" => "1871020202960003",
            "family_card" => "1871020603070009",
            "full_name" => "FERI KRISNANTO",
            "religion" => "ISLAM",
            "job" => "BELUM/TIDAK BEKERJA",
            "education" => "SLTA/SEDERAJAT",
            "date_of_birth" => "02-02-1996",
            "place_of_birth" => "SURABAYA",
            "marital_status" => "BELUM KAWIN",
            "blood_group" => "TIDAK TAHU",
            "gender" => "Laki-Laki",
            "mother_name" => "DJAMILAH"
        ];

        if($nik == '1871020202960002'){
            return $data;
        }elseif ($nik == '1871020202960003') {
            return $data2;
        }else{
            return [];
        }

    }
}
