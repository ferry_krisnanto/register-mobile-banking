<?php

namespace App\Http\Controllers\SSO;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

use App\Services\Sso\AuthServer;
use App\Models\User;
use Exception;

class IndexController extends Controller
{
    protected $authServer;

    public function __construct(AuthServer $authServer)
    {
        $this->middleware('guest')->except('logout');

        $this->authServer = $authServer;
    }

    public function index()
    {
        if(Auth::check()){
            return redirect()->route('home');
        }
        return view('auth.login');
    }

    public function login(Request $request) {

        // s: Get Service Auth
        $client = $this->authServer->login($request->username, $request->password);
        $response = $client->getData();
        // e: Get Service Auth

        // s: Prosessing response Service Auth
        if($response->code >= 200 && $response->code < 300 && !empty($response->data)){
            try {
                $data = $response->data;
                $role = $data->roles;
                if(blank($role)){
                    return $this->errorResponse('Anda Tidak Memiliki Akses');
                }
                
                if(
                    permission($role, "[PUNIC][INITIATOR]") || 
                    permission($role, "[PUNIC][OTORISATOR]"))
                {
                    // Continue
                }else{
                    return $this->errorResponse('Anda Tidak Memiliki Akses');
                }

                if(blank($data->department)){
                    return $this->errorResponse('Unit Kerja Kosong, Silahkan Update Data anda');
                }

                $username   = $request->username;
                $password   = $request->password;
                $full_name  = $data->full_name;
                $position   = $data->position;
                $role       = $role;
                $identity   = $data->identity;
                $email      = $data->email;
                $department = $data->department;
                $token      = $data->access_token;

                // s: Upsert Data
                $userUpsert = User::updateOrCreate(
                [
                    'username' => $username,
                ],
                [
                    'npp'      => $identity,
                    'username' => $username,
                    'email'    => $email,
                    'full_name'=> $full_name,
                    'password' => bcrypt($password),
                    'position' => $position,
                    'role'     => $role,
                    'departement' => $department,
                    'token'    => $token,
                ]);
                // e: Upsert Data

                // s: Check Login
                if(Auth::attempt([
                    'username' => $username,
                    'password' => $password
                ])){
                    $request->session()->regenerate();
                    return redirect()->intended('home');
                }else{
                    return $this->errorResponse('Gagal Login, Silahkan Ulangi Kembali');
                }
                // e: Check Login

            } catch (Exception $e) {
                return $this->errorResponse("Gagal Login, {$e->getMessage()}");
            }
        }else{
            return $this->errorResponse('Gagal Login, Silahkan Cek User Password.');
        }
        // e: Prosessing response Service Auth
    }

    private static function successResponse($data, $message, $statusCode)
    {
        return response()->json([
            'code' => $statusCode,
            'data' => $data,
            'message' => $message
        ], $statusCode);
    }

    private function errorResponse($message)
    {
        return redirect()
            ->back()
            ->with(['failed' => $message ]);
    }

    public function logout(Request $request, AuthServer $authServer)
    {
        // $token = Auth::user()->token;
        // $this->authServer->logout($token);

        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
    }
}
