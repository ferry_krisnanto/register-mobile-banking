<?php

namespace App\Services\Connection;

class Response {

    public static function success($data, $message, $statusCode)
    {
        return response()->json([
            'code' => $statusCode,
            'data' => $data,
            'message' => $message
        ], $statusCode);
    }

    public function error($message, $statusCode = 500)
    {
        return response()->json([
            'code' => $statusCode,
            'error' => $message,
        ], $statusCode);
    }

    public function errorResponse($message, $statusCode = 500)
    {
        return response()->json([
            'code' => $statusCode,
            "success" => false,
            'message' => [
                'indonesian' => $message,
                'english'   => $message
            ],
        ], $statusCode);
    }

}