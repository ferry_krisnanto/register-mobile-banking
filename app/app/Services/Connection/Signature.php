<?php

namespace App\Services\Connection;

use Illuminate\Support\Str;

class Signature {

    private $key;

    public function __construct()
    {
        $this->key = env('PUNIC_KEY');
    }

    public function get($method, $url, $accessToken, $body, $timeStamp)
    {
        $bodyStr = $this->toPayLoad($body);
        $signature = "$method:$url:$accessToken:$bodyStr:$timeStamp";
        return $hash = $this->hashHmac($signature);
    }

    private function toPayLoad($body)
    {
        $string = Str::camel($body);
        $newString = $this->hashSha256($string);
        return $newString;
    }

    private function hashSha256($string, $type = 'sha256')
    {
        return hash('sha256', $string);
    }

    private function hashHmac($string)
    {
        return hash_hmac('sha256', $string, $this->key);
    }
}