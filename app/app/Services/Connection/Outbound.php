<?php

namespace App\Services\Connection;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

use Illuminate\Support\Facades\Http;

use App\Services\Connection\Response;

class Outbound {

    private $baseUrl;
    public $key;

    public function __construct(Response $response)
    {
        $this->baseUrl      = env('PUNIC_URL');
        $this->key          = env('PUNIC_KEY');
        $this->res          = $response;

        $this->client       = new Client([
            'base_uri' => $this->baseUrl
        ]);

        // mock
        $this->mockUrl      = env('MOCK_URL');
        $this->clientMock   = new Client([
            'base_uri' => $this->mockUrl
        ]);
    }

    public function setHeader($token, $timeStamp, $signature)
    {
        $headers = [
            'Accept: application/json',
            "Authorization" => "Bearer $token",
            "Content-Type" => "application/json",
            "X-BLPG-Key" => $this->key,
            "X-BLPG-Timestamp" => $timeStamp,
            "X-BLPG-Signature" => $signature,
        ];
        return $headers;
    }

    public function sendRequest($method, $path, $headers, $body = [])
    {
        if (is_array($headers) && is_array($body)) {
            $data = [
                'headers' => $headers,
                'body' => json_encode($body),
            ];
            return $this->request($method, $path, $data);
        }else{
            return $this->res->error('Laravel Client: Request miss match', $statusCode = 500);
        }
    }

    private function request($method, $path, $data)
    {
        try {
            $request = $this->client->request($method, $path, $data);
            $response = (string) $request->getBody();

            if(is_object($response)){
                return $response;
            }else{
                return $response = json_decode($response);
            }

        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $exception = (string) $e->getResponse()->getBody();
            }else{
                return $this->res->error('failed', $statusCode = 500);
            }
        }
    }

    public function conversion($response)
    {
        if(is_object($response)){
            return $response;
        }else{
            return json_decode($response);
        }
    }

    // Mock
    public function sendRequestMock($method, $path, $headers, $body = [])
    {
        if (is_array($headers) && is_array($body)) {
            $data = [
                'headers' => $headers,
                'body' => json_encode($body),
            ];
            return $this->requestMock($method, $path, $data);
        }else{
            return $this->res->error('Laravel Client: Request miss match', $statusCode = 500);
        }
    }

    private function requestMock($method, $path, $data)
    {
        // return $path;
        try {
            $request = $this->clientMock->request($method, $path, $data);
            $response = (string) $request->getBody();

            if(is_object($response)){
                return $response;
            }else{
                return $response = json_decode($response);
            }

        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $exception = (string) $e->getResponse()->getBody();
            }else{
                return $this->res->error('failed', $statusCode = 500);
            }
        }
    }
}