<?php

namespace App\Services\Connection;

use Auth;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

use Illuminate\Support\Facades\Http;
use App\Services\Connection\Response;

class SendRequest {

    private $env;
    private $version;

    public function __construct($env, $version){

        $this->env      = $env;  
        $this->version  = $version;

        $this->baseUrl  = env($this->env.'_URL');
        $this->client   = new Client([
            'base_uri' => $this->baseUrl
        ]);

    }

    public function get($path)
    {
        $newPath = $this->version .'/'. $path;
        $response = $this->request('GET', $newPath, []);
        return $this->convert($response);
    }

    public function getWithAuth($path)
    {
        $token = Auth::user()->token;
        $payloads = [
            'headers' => [
                'Accept: application/json',
                "Authorization" => "Bearer $token",
                "Content-Type" => "application/json"
            ],
        ];
        
        $newPath = $this->version .'/'. $path;
        $response = $this->request('GET', $newPath, $payloads);
        return $this->convert($response);
    }

    private function request($method, $path, $payloads)
    {
        try {
            $request = $this->client->request($method, $path, $payloads);
            $response = (string) $request->getBody();
            return $this->convert($response);

        } catch (RequestException $e) {
            if ($e->hasResponse()) {
                return $exception = (string) $e->getResponse()->getBody();
            }else{
                return $this->res->error('failed', $statusCode = 500);
            }
        }
    }

    private function convert($response)
    {
        if(is_object($response)){
            return $response;
        }else{
            return $response = json_decode($response);
        }
    }

}