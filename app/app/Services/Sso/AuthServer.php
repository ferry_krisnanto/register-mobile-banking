<?php

namespace App\Services\SSO;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\RequestException;

class AuthServer {

    private $baseUrl;

    private $clientId;
    private $clientSecret;
    private $grantType;
    private $client;

    public function __construct()
    {
        $this->baseUrl      = env('PUNIC_URL');
        $this->clientId     = env('PUNIC_USERNAME');
        $this->clientSecret = env('PUNIC_PASSWORD');
        $this->grantType    = env('PUNIC_GRANT_TYPE');

        $this->client       = new Client([
            'base_uri' => $this->baseUrl
        ]);
    }

    public function login($username, $password)
    {

        $credentials = base64_encode($this->clientId . ':' . $this->clientSecret);
        $headers = [
            'Authorization' => ['Basic '.$credentials],
            'Accept: application/json',
            "Content-Type" => "application/x-www-form-urlencoded",
        ];

        $params = [
            'grant_type' => $this->grantType,
            'username' => $username,
            'password' => $password,
        ];

        return $this->post('/auth/oauth/token', [
            'headers' => $headers,
            'form_params' => $params,
        ]);
    }

    public function logout($token)
    {
        $credentials = base64_encode($this->clientId . ':' . $this->clientSecret);
        $headers = [
            "Authorization" => "Bearer $token",
            'Accept: application/json',
            "Content-Type" => "application/x-www-form-urlencoded",
        ];
        $params = [];
        return $this->post('/oauth/revoke', [
            'headers' => $headers,
            'form_params' => $params,
        ]);
    }

    public function post($path, $data = [])
    {
        return $this->request('POST', $path, $data);
    }

    public function get($path, $data = []) {
        return $this->request('GET', $path, $data);
    }

    public function request($method, $path, $data)
    {
        try {
            $request = $this->client->request($method, $path, $data);

            $response = (string) $request->getBody();
            $response = $this::decode($response);
            return $this::successResponse($response, 'Berhasil login', $request->getStatusCode());
        } catch (RequestException $e) {
            // if ($e->hasResponse()) {
            //     $exception = (string) $e->getResponse()->getBody();
            //     $exception = $this::decode($exception);

            //     $message = "Unknown";

            //     if (!empty($exception) && $this->validObject($exception->error) &&
            //         $this->validObject($exception->error_description)) {
            //         $message =  "[" . $exception->error ."] " . $exception->error_description;
            //     }

            //     return $this::errorResponse($message, $e->getCode());
            // } else {
            //     $message = $e->getMessage();

            //     return $this::errorResponse($message, 503);
            // }
            $message = $e->getMessage();
            return $this::errorResponse($message, 503);

        } catch (\GuzzleHttp\Exception\GuzzleException $e) {
            $message = $e->getMessage();

            return $this::errorResponse($message, 500);
        }
    }

    private static function validObject($obj)
    {
        return !is_object($obj);
    }

    private static function decode($value)
    {
        return json_decode($value);
    }

    private static function successResponse($data, $message, $statusCode)
    {
        return response()->json([
            'code' => $statusCode,
            'data' => $data,
            'message' => $message
        ], $statusCode);
    }

    private function errorResponse($message, $statusCode = 500)
    {
        return response()->json([
            'code' => $statusCode,
            'error' => $message,
        ], $statusCode);
    }

}
