<?php

namespace App\Services\Date;

use Carbon\Carbon;

class Date {

    public function dateAtom()
    {
        $datetime = Carbon::now();
        $datetime->shiftTimezone('Asia/Jakarta');
        $date = $datetime->toAtomString();

        return $date;
    }

    public function dateRfc3339(Type $var = null)
    {
        $datetime = Carbon::now();
        $datetime->shiftTimezone('Asia/Jakarta');
        $date = $datetime->toRfc3339String(true);

        return $date;
    }
}