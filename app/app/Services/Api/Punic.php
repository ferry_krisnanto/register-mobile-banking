<?php

namespace App\Services\Api;

use Auth;
use App\Services\Connection\Outbound;

class Punic {

    public function __construct(Outbound $outbound)
    {
        $this->out  = $outbound;
    }

    public function v1($slug, $method = 'GET', $body = [])
    {
        $url = "punic/$slug";
        $token = Auth::user()->token;
        $header = [
            'Accept: application/json',
            "Authorization" => "Bearer $token",
            "Content-Type" => "application/json"
        ];
        $response = $this->out->sendRequest($method, $url, $header, $body);
        return $this->convert($response);
    }

    public function v2($slug, $method = 'GET', $body = [])
    {
        $url = "v2/punic/$slug";
        $token = Auth::user()->token;
        $header = [
            'Accept: application/json',
            "Authorization" => "Bearer $token",
            "Content-Type" => "application/json"
        ];
        $response = $this->out->sendRequest($method, $url, $header, $body);
        return $this->convert($response);
    }

    public function pinDefault($id)
    {
        return $response = $this->v1("proposals/$id/default-cred"); 
    }

    public function listProposals($page, $doc_type, $application, $log_code='')
    {
        $size = 10;

        if(isOtorisator()){
            $log_code = 'PNC_001';
        }
        
        $proposals = $this->v2("proposals?page=$page&item_per_page=$size&doc_type=$doc_type&application=$application&sort_by=createdDate:DESC&log_code=$log_code");
        return $proposals;
    }

    public function submitOtorisator($accountSelected)
    {
        // Get Data
        $response = $this->requestOtorisator();
        $users = $response->data->content;
        $newUser = collect($users)->whereIn('id', $accountSelected);
        $count = $newUser->count();

        // Parse Data
        $body = [];
        foreach ($newUser as $index => $value) {
            $body['otorisators'][] = [
                "username"  => $value->user->username,
                "full_name" => $value->full_name,
                "position"  => $value->position ?? '-',
                "department"=> [
                    "code"  => $value->department->code,
                    "name"  => $value->department->name,
                    "type"  => $value->department->type ?? '-'
                ]
            ];
            
        };
        return $body;
    }

    public function requestOtorisator()
    {
        $method = 'GET';
        $token      = Auth::user()->token;
        $depCode    = Auth::user()->departement['code'];
        $url        = "auth/employes?page=0&size=20&search=roleApp;PUNIC AND departCode;$depCode AND role;OTORISATOR";
        $header = [
            'Accept: application/json',
            "Authorization" => "Bearer $token",
            "Content-Type" => "application/json"
        ];
        $response = $this->out->sendRequest($method, $url, $header);
        return $this->convert($response);
    }

    private function convert($response)
    {
        if(is_object($response)){
            return $response;
        }else{
            return $response = json_decode($response);
        }
    }

    public function sendOtp($body)
    {
        return $response = $this->v1('customers/otp', $method = 'POST', $body); 
    }

    public function verifOtp($otp, $draftId)
    {
        return $response = $this->v1("customers/otp?otp=$otp&draft_id=$draftId");
    }
}