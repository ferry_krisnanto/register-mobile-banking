<?php

namespace App\Services\Api;

use Auth;
use App\Services\Connection\SendRequest;

class Endpoint {

    public static function areca($version)
    {   
        $env = 'ARECA';
        return $response = new SendRequest($env, $version);
    }

    public static function punic($version)
    {   
        $env = 'PUNIC';
        return $response = new SendRequest($env, $version);
    }

    public static function gateway($version)
    {   
        $env = 'GATEWAY';
        return $response = new SendRequest($env, $version);
    }
}