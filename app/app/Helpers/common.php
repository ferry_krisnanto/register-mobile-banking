<?php

function setActive($uri, $output = 'active')
{
	if( is_array($uri) ) {
		foreach ($uri as $u) {
			if(Route::is($u)) {
				return $output;
			}
		}
	} else {
		if(Route::is($uri)) {
			return $output;
		}
	}
}

function permission($roles, $roleName)
{
	foreach($roles as $role){
		if($role == $roleName){
			return true;
		}
	}
	return false;
}


function isInitiator(){
	$roleName = config('static.role.initiator');
	try {
		$role = auth()->user()->role;
		return permission($role, $roleName);
	} catch (\Throwable $th) {
		return false;
	}
}

function isOtorisator(){
	$roleName = config('static.role.otorisator');
	try {
		$role = auth()->user()->role;
		return permission($role, $roleName);
	} catch (\Throwable $th) {
		return false;
	}
}
