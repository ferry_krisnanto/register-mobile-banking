<?php

namespace App\Models;

use Jenssegers\Mongodb\Eloquent\Model;

class Customer extends Model
{
    protected $connection = 'mongodb';
    protected $primaryKey = '_id';
    protected $collection= 'customers';
}
