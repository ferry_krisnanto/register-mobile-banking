<?php

return [
    'version_app'   => '1.5.2',
    'type'          => env('APP_ENV', 'RELEASE'),

    'product'   => ['01', '02', '03', '04', '05', '06', '07', '24', '88'],

    'role' => [
        'initiator' => '[PUNIC][INITIATOR]',
        'otorisator' => '[PUNIC][OTORISATOR]'
    ],

    'religion' => [
        'ISLAM',
        'KRISTEN',
        'KATHOLIK',
        'HINDU',
        'BUDHA',
        'KONGHUCU'
    ],

    'marital'   => [
        "KAWIN",
        "BELUM KAWIN",
        "CERAI MATI",
        "CERAI HIDUP"
    ],

    'education' => [
        "TIDAK/BLM SEKOLAH",
        "BELUM TAMAT SD/SEDERAJAT",
        "TAMAT SD/SEDERAJAT",
        "SLTP/SEDERAJAT",
        "SLTA/SEDERAJAT",
        "AKADEMI/DIPLOMA III/SARJANA MUDA",
        "DIPLOMA I/II",
        "DIPLOMA IV/STRATA I",
        "STRATA-II",
        "STRATA-III",
        "LAINNYA",
    ],

    'profession'    => [
        "APOTEKER",
        "ARSITEK",
        "ANGGOTA DPRD KAB.",
        "BELUM/TIDAK BEKERJA",
        "BIDAN",
        "BURUH TANI/PERKEBUNAN",
        "BURUH HARIAN LEPAS",
        "BURUH PETERNAKAN",
        "BURUH NELAYAN/PERIKANAN",
        "DOKTER",
        "DOSEN",
        "GURU",
        "INDUSTRI",
        "KARYAWAN BUMD",
        "KARYAWAN BUMN",
        "KARYAWAN HONORER",
        "KARYAWAN SWASTA",
        "KEPOLISIAN RI (POLRI)",
        "KEPALA DESA",
        "KONSTRUKSI",
        "KONSULTAN",
        "MENGURUS RUMAH TANGGA",
        "MEKANIK",
        "NELAYAN/PERIKANAN",
        "NOTARIS",
        "PEDAGANG",
        "PEGAWAI NEGERI SIPIL (PNS)",
        "PENDETA",
        "PENSIUNAN",
        "PENELITI",
        "PENGACARA",
        "PENATA RIAS",
        "PETERNAK",
        "PERANGKAT DESA",
        "PERAWAT",
        "PERDAGANGAN",
        "PETANI/PEKEBUN",
        "PELAJAR/MAHASISWA",
        "PILOT",
        "SOPIR",
        "SENIMAN",
        "TENTARA NASIONAL INDONESIA (TNI)",
        "TRANSPORTASI",
        "TUKANG BATU",
        "TUKANG KAYU",
        "TUKANG LAS/PANDAI BESI",
        "TUKANG LISTRIK",
        "TUKANG JAHIT",
        "PARAJI",
        "USTADZ/MUBALIGH",
        "WARTAWAN",
        "WIRASWASTA",
        "LAINNYA"
    ],
];
