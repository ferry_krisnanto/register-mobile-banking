<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Sso\IndexController as Auth;
use App\Http\Controllers\HomeController as Home;
use App\Http\Controllers\Blokir\IndexController as Blokir;
use App\Http\Controllers\Register\IndexController as Register;
use App\Http\Controllers\Register\InquiryController as RegisterInquiry;
use App\Http\Controllers\Register\ListController as ListController;
use App\Http\Controllers\ChangePin\IndexController as ChangePin;
use App\Http\Controllers\ChangePhone\IndexController as ChangePhone;
use App\Http\Controllers\ChangeDevice\IndexController as ChangeDevice;

use App\Http\Controllers\OtorisatorController as Otorisator;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('login');
});

Route::get('/login', [Auth::class, 'index']);
Route::post('login', [Auth::class, 'login'])->name('login');
Route::post('logout', [Auth::class, 'logout'])->name('logout');

Route::get('/province', [Region::class, 'province'])->name('Region.province');
Route::get('/regency', [Region::class, 'regency'])->name('Region.regency');
Route::get('/district', [Region::class, 'district'])->name('Region.district');
Route::get('/village', [Region::class, 'village'])->name('Region.village');

Route::middleware(['auth', 'prevent-back-history'])->group(function(){

    Route::get('/home', [Home::class, 'index'])->name('home');
    Route::get('/mock/{nik}', [Home::class, 'mock'])->name('mock');
    Route::get('/areca/inquiry', [Home::class, 'mockInquiry']);
    Route::get('/get-auth', [Home::class, 'getToken']);

    // Start: Register Mobile Banking - NEW VERSION
    Route::get('/register-mobile-banking', [RegisterInquiry::class, 'index'])->name('register.index');
    Route::post('/register-mobile-banking/inquiry', [RegisterInquiry::class, 'inquiry'])->name('RegisterInquiry.inquiry');
    Route::post('/register-mobile-banking/submit-draft', [Register::class, 'submitDraft'])->name('submit.draft');
    
    Route::get('/register-mobile-banking/{id}/step-2', [RegisterInquiry::class, 'step2'])->name('RegisterInquiry.step2');
    Route::post('/register-mobile-banking/send-otp', [RegisterInquiry::class, 'sendOtp'])->name('RegisterInquiry.sendOtp');
    Route::post('/register-mobile-banking/validate-otp', [RegisterInquiry::class, 'validateOtp'])->name('RegisterInquiry.validateOtp');

    Route::get('/register-mobile-banking/{id}/step-3', [RegisterInquiry::class, 'step3'])->name('RegisterInquiry.step3');
    Route::post('/register-mobile-banking/get-account-by-cif', [Register::class, 'getAccountByCif'])->name('Register.getAccountByCif');
    Route::post('/register-mobile-banking/save-account', [Register::class, 'saveAccount'])->name('Register.saveAccount');

    Route::get('/register-mobile-banking/otorisator/{id}', [RegisterInquiry::class, 'otorisator'])->name('Register.otorisator');
    Route::post('/register-mobile-banking/submit-proposal/{id}', [RegisterInquiry::class, 'submit'])->name('Register.submit');
    // End: Register Mobile Banking - NEW VERSION

    // Start: Register Mobile Banking - Proposals
    Route::get('/register-mbanking/proposal', [ListController::class, 'listProposal'])->name('list.index');
    Route::get('/register-mbanking/draft', [ListController::class, 'listDraft'])->name('list.draft');
    Route::get('/register-mbanking/detail/{id}', [ListController::class, 'detail'])->name('list.id');
    Route::get('/register-mbanking/detail/{id}/continue', [ListController::class, 'detailforCs'])->name('list.cs');
    Route::get('/register-mobile-banking/{id}/check-step', [ListController::class, 'checkStep'])->name('list.checkStep');
    // END: Register Mobile Banking - Proposals


    

    // Start: Register Mobile Banking - V.1.0
    Route::get('/register-account/{id}', [Register::class, 'registerAccount'])->name('register.account');
    Route::get('/send-otp/{id}', [Register::class, 'sendOtp'])->name('register.sendotp');
    // Route::get('/choose-otorisator/{id}', [Register::class, 'chooseOtorisator'])->name('choose.otorisator');
    Route::post('/register-final/{id}', [Register::class, 'registerFinal'])->name('register.final');
    Route::post('/apis/send/{id}', [ListController::class, 'approveProposal'])->name('send.proposal');


    Route::post('/apis/draft', [Register::class, 'createDraft'])->name('register.draft');
    
    Route::post('/apis/get-account-cif', [Register::class, 'getAccountByCif'])->name('account.cif');
    Route::post('/apis/submit-account', [Register::class, 'submitAccount'])->name('submit.account');
    Route::post('/apis/send-otp', [Register::class, 'apiSendOtp'])->name('send.otp');
    Route::post('/apis/verif-otp', [Register::class, 'apiVerifOtp'])->name('verif.otp');

    
    // End: Register Mobile Banking - V.1.0

    // Start: Change PIN
    Route::get('/change-pin', [ChangePin::class, 'index'])->name('changePin.index');
    Route::post('/change-pin', [ChangePin::class, 'inquiry'])->name('changePin.inquiry');
    Route::post('/change-pin/create-proposal', [ChangePin::class, 'proposal'])->name('changePin.proposal');
    Route::get('/change-pin/otorisator/{id}', [ChangePin::class, 'otorisator'])->name('changePin.otorisator');
    Route::post('/change-pin/submit-proposal/{id}', [ChangePin::class, 'submit'])->name('changePin.submit');
    Route::get('/change-pin/drafts', [ChangePin::class, 'listDraft'])->name('changePin.drafts');
    Route::get('/change-pin/proposals', [ChangePin::class, 'listProposal'])->name('changePin.proposals');
    Route::get('/change-pin/detail/{id}', [ChangePin::class, 'detail'])->name('changePin.detail');
    // End: Change PIN


    // Start: Change Phone Number
    Route::get('/change-phone', [ChangePhone::class, 'index'])->name('changePhone.index');
    Route::post('/change-phone', [ChangePhone::class, 'inquiry'])->name('changePhone.inquiry');
    Route::post('/change-phone/create-proposal', [ChangePhone::class, 'proposal'])->name('changePhone.proposal');
    Route::get('/change-phone/verif-phone/{id}/{phone}', [ChangePhone::class, 'verifPhone'])->name('changePhone.verifPhone');
    Route::post('/change-phone/verify-otp', [ChangePhone::class, 'verify'])->name('changePhone.verify');
    Route::post('/change-phone/resend-otp', [ChangePhone::class, 'resendOtp'])->name('changePhone.resendOtp');
    Route::get('/change-phone/otorisator/{id}', [ChangePhone::class, 'otorisator'])->name('changePhone.otorisator');
    Route::post('/change-phone/submit-proposal/{id}', [ChangePhone::class, 'submit'])->name('changePhone.submit');
    Route::get('/change-phone/drafts', [ChangePhone::class, 'listDraft'])->name('changePhone.drafts');
    Route::get('/change-phone/proposals', [ChangePhone::class, 'listProposal'])->name('changePhone.proposals');
    Route::get('/change-phone/detail/{id}', [ChangePhone::class, 'detail'])->name('changePhone.detail');
    // End: Change PIN

    // Start: Blokir
    Route::get('/blokir', [Blokir::class, 'index'])->name('blokir.index');
    Route::post('/blokir', [Blokir::class, 'inquiry'])->name('blokir.inquiry');
    Route::post('/blokir/create-proposal', [Blokir::class, 'proposal'])->name('blokir.proposal');
    Route::get('/blokir/otorisator/{id}', [Blokir::class, 'otorisator'])->name('blokir.otorisator');
    Route::post('/blokir/submit-proposal/{id}', [Blokir::class, 'submit'])->name('blokir.submit');

    Route::get('/blokir/drafts', [Blokir::class, 'listDraft'])->name('blokir.drafts');
    Route::get('/blokir/proposals', [Blokir::class, 'listProposal'])->name('blokir.proposals');
    Route::get('/blokir/detail/{id}', [Blokir::class, 'detail'])->name('blokir.detail');
    // End: Blokir


    // Start: Change Device
    Route::get('/change-device', [ChangeDevice::class, 'index'])->name('changeDevice.index');
    Route::post('/change-device', [ChangeDevice::class, 'inquiry'])->name('changeDevice.inquiry');
    Route::post('/change-device/create-proposal', [ChangeDevice::class, 'proposal'])->name('changeDevice.proposal');
    Route::get('/change-device/otorisator/{id}', [ChangeDevice::class, 'otorisator'])->name('changeDevice.otorisator');
    Route::post('/change-device/submit-proposal/{id}', [ChangeDevice::class, 'submit'])->name('changeDevice.submit');

    Route::get('/change-device/drafts', [ChangeDevice::class, 'listDraft'])->name('changeDevice.drafts');
    Route::get('/change-device/proposals', [ChangeDevice::class, 'listProposal'])->name('changeDevice.proposals');
    Route::get('/change-device/detail/{id}', [ChangeDevice::class, 'detail'])->name('changeDevice.detail');
    // End: Change Phone Number


    // S: Otorisator Approval
    Route::post('/aproval-proposal/otorisator/{id}', [Otorisator::class, 'aprovalProposal'])->name('otorisator.aprovalProposal');
    // E: Otorisator
});


Route::get('/test', [Register::class, 'test'])->name('test');